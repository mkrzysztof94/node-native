/**
 * @link '../src/lib/src/Super'
 * @class Super
 */
export class Super {
    constructor(obj: Object)
}

declare module "MultiThreads" {
    export interface IpcMessageInterface {
        eval: String;
        log: String;
    }
    /**
     * @link '../src/MultiThreads/IpcMessage'
     */
    export class IpcMessage extends Super{
        constructor(Input: IpcMessageInterface)
        parseEvals(): IpcMessage;
        parseLogs(): IpcMessage;
        run(): void;
        addLog(log: String): IpcMessage;
        addEval(evalStr: String): IpcMessage;
        addToLog(logStr: String): void;
        addToEval(evalStr: String): void;
        toRun(): Function[];
        static parse(msg: String): void;
        static getTemplate(): IpcMessage;
        toSend(): void;
    }

}
const Empty = require('./Empty')
/**
 * 
 * 
 * @class BaseClass
 * @extends {Empty}
 */
class BaseClass extends Empty {
    /**
     * Creates an instance of BaseClass.
     * @param {any} [init={}] 
     * @param {any} inherit 
     * @memberof BaseClass
     */
    constructor(init = {}, inherit) {
        if (!new.target) throw 'Class constructor must be called with new'
        super()
        if (typeof inherit === 'object') {
            let self = Object.setPrototypeOf(this, inherit)
            return self
        }
        return Object.assign(this, init)
    }
}

module.exports = BaseClass

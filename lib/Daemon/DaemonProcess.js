const childProcess = require('child_process')
const fs = require('fs')
const loginKeys = require('./LoginKeys')
const Pipe = require('./Pipe')
const path = require('path')
const PipeStreams = require('./PipeStreams')
const pipePath = path.join('\\\\?\\pipe', process.cwd(), 'daemonN')

/**
 * @description Creates Daemon process
 * @class DaemonProcess
 * @requires fs,loginKeys,Pipe
 */
class DaemonProcess {
    /**
     * @description Function parameter should be path to whole project
     * Creates an instance of DaemonProcess.
     * @param {string} packageDirPath 
     * @memberof DaemonProcess
     */
    constructor(packageDirPath) {
        this._process = process
        this._packageDirPath = packageDirPath
        this._daemonLogDirPath = path.resolve(packageDirPath, 'logs/daemon')
        let streams = DaemonProcess.createPipeStreams(this._daemonLogDirPath)
        this._pipe = new Pipe(streams, pipePath)
        this._process.on('error', args => this._pipe.write('[Daemon::Error]', JSON.stringify(args)))

        this._pipe.streams._input.on('data', args => this._pipe.write('[PipeServInput::Data]' + JSON.stringify(args)))
        this._pipe.start()
        process.on('exit', () => {
            process.kill(process.pid)
            fs.unlinkSync(path.resolve(packageDirPath, 'tmp/daemon.pid'))
        })
    }

    /**
     * @description Function returns demon constant Key
     * @readonly
     * @returns {string}
     * @memberof DaemonProcess
     */
    get Key() {
        return loginKeys.daemon
    }

    /**
     * @description Process main loop
     * @static
     * @returns {async}
     * @memberof DaemonProcess
     */
    static daemonLoop() {
        setTimeout(() => DaemonProcess.daemonLoop(), 1000)
    }

    /**
     * @description 
     * @static
     * @param {string} packageDirPath 
     * @param {array} [args=[]] 
     * @param {object} [options={}] 
     * @returns  {void|DaemonProcess}
     * @memberof DaemonProcess
     */
    static Spawn(packageDirPath, args = [], options = {}) {
        if (process.env.__daemon === undefined) {
            let scriptPath = path.resolve(packageDirPath + '/runDaemon.js')
            args.push(scriptPath)
            options = DaemonProcess.getDefaultSpawnOptions(options)
            let daemon = childProcess.spawn(process.execPath, args, options)
            let processObj = {
                pid: daemon.pid,
                pipe: pipePath
            }
            console.log(JSON.stringify(processObj))
            daemon.unref()
        } else {
            process.env.__daemon = process.pid
            return new DaemonProcess(path.resolve(packageDirPath, '../'))
        }
    }

    /**
     * @static
     * @param {Object} [options={}] 
     * @returns  {Object}
     * @memberof DaemonProcess
     */
    static getDefaultSpawnOptions(options = {}) {
        options = Object.assign(
            {
                stdio: 'pipe',
                env: Object.create(process.env),
                cwd: process.cwd(),
                detach: true
            },
            options
        )
        options.env.__daemon = true
        return options
    }

    /**
     * @static
     * @param {string} dirPath 
     * @returns  {PipeStreams}
     * @memberof DaemonProcess
     */
    static createPipeStreams(dirPath) {
        let fsOptions = {
            flags: 'a+',
            encoding: 'utf8'
        }

        let inp = fs.createReadStream(`${dirPath}/input.sock`, fsOptions)
        let out = fs.createWriteStream(`${dirPath}/out.log`, fsOptions)
        let err = fs.createWriteStream(`${dirPath}/out.log`, fsOptions)

        inp.on('error', args => out.write(args))
        out.on('error', args => out.write(args))
        err.on('error', args => out.write(args))

        return new PipeStreams(inp, out, err)
    }
}

module.exports = DaemonProcess

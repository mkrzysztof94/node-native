/**
 * @namespace Daemon
 * @constant loginKeys
 */
const loginKeys = {
    debug: '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8',
    daemon: '03dc556436c1c9e3f1e86ad9a53e79eabbcc6cc9cc8f4a8eb3beda6484ad18d1'
}

module.exports = loginKeys

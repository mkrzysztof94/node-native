const net = require('net')
const path = require('path')
const loginKeys = require('./LoginKeys')
const PipeStreams = require('./PipeStreams')
/**
 * @namespace Daemon
 * @module Daemon.Pipe
 * @class Pipe
 */
class Pipe {
    /**
     * Creates an instance of Pipe.
     * @param {PipeStreams} pipeStreams 
     * @param {string} pipePath
     * @constructor 
     * @memberof Pipe
     */
    constructor(pipeStreams, pipePath) {
        if (!(pipeStreams instanceof PipeStreams)) {
            throw Error('Pipe must be constructed with PipeStreams instance')
        }
        this.streams = pipeStreams
        this.clients = {
            debug: null
        }
        this.server = net.createServer(sock => this.createServerClient(sock))
        this._path = pipePath
        this.server.on('error', error =>
            this.defaultEventHandler('Server', 'error', error)
        )
        this.server.on('data', data =>
            this.defaultEventHandler('Server', 'data', data)
        )
    }

    /**
     * @description Return path
     * @returns {string}
     * @memberof Pipe
     */
    getPath() {
        return this._path
    }

    /**
     * @description Start server listing
     * @returns {void}
     * @memberof Pipe
     */
    start() {
        this.server.listen(this.getPath())
    }

    /**
     * 
     * 
     * @param {any} client 
     * @memberof Pipe
     */
    createServerClient(client) {
        client._name = 'NewClient'
        client.setEncoding('utf-8')
        client.on('error', data =>
            this.defaultEventHandler(client._name, 'error', data)
        )

        let loginTimeout = setTimeout(() => {
            client.emit('error', 'Too long login time !')
            client.end('Too long login time !')
        }, 1000)

        client.once('data', args => {
            client.key = String(args)
            clearTimeout(loginTimeout)
            if (client.key === loginKeys.debug) {
                client._name = 'Debugger'
                this.write('[Debugger::Event] Connected')
                client.setKeepAlive(true, 5000)
                client.on('close', data =>
                    this.defaultEventHandler(client._name, 'close', data)
                )
                client.on('end', data => {
                    this.defaultEventHandler(client._name, 'end', data)
                    this.clients.debug._destroy()
                    this.clients.debug = null
                })
                this.clients.debug = client
                client.on('data', data =>
                    this.onDebugData(this.clients.debug, data)
                )
            } else {
                client.debug.setKeepAlive(false, 0)
                client.end()
            }
        })
    }

    /**
     * @description Default event handler
     * @param {string} authorName 
     * @param {string} eventName 
     * @param {string} [data=''] 
     * @returns {void}
     * @memberof Pipe
     */
    defaultEventHandler(authorName, eventName, data = '') {
        this.write(`[${authorName}::${eventName}] ==> ${data}`)
    }

    /**
     * @event Debug.onData
     * @param {string} author 
     * @param {string} data 
     * @returns {void} 
     * @memberof Pipe
     */
    onDebugData(author, data) {
        let parsedData = ''
        try {
            parsedData = JSON.parse(data)
            if (
                parsedData.type === undefined ||
                parsedData.data === undefined
            ) {
                throw Error('undefined object !')
            }
            this.handleMessage(author, parsedData)
        } catch (error) {
            this.write(error.message)
            try {
                parsedData = String(data)
                this.write(`[Server:onDebugData] <==${parsedData}`)
            } catch (err) {
                this.streams._output.write(err.message)
            }
        }
    }

    /**
     * @description Alias to output write
     * @param {string} str 
     * @returns  {boolean}
     * @memberof Pipe
     */
    write(str) {
        return this.streams._output.write(`\r${Pipe.getDateTimeStr()}${str}`)
    }

    /**
     * @description Timestamp creator
     * @static
     * @returns  {string}
     * @memberof Pipe
     */
    static getDateTimeStr() {
        let nowDate = new Date(Date.now())
        return `[${nowDate.toLocaleDateString()} ${nowDate.toLocaleTimeString()}]`
    }

    /**
     * @description Message handler
     * @event onMessage
     * @param {string} author 
     * @param {Object} dataObj 
     * @returns  {void}
     * @memberof Pipe
     */
    handleMessage(author, dataObj) {
        switch (dataObj['type']) {
        case 'message':
            this.write(`[Server::Debug::Message] ===>> ${dataObj.data}`)
            author.write('[Server::Response]:[Message::Recived]')
            break
        case 'exit':
            return process.exit(0)
        default:
            author.write('[Server::Response]:[Message::Undefined]')
            break
        }
    }
}

module.exports = Pipe

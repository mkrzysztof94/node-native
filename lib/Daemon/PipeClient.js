const net = require('net')
const loginKeys = require('./LoginKeys')
/**
 * @description Helper class for daemon debug
 * @namespace Daemon
 * @module Daemon.PipeClient
 * @class PipeClient
 */
class PipeClient {
    /**
     * Creates an instance of PipeClient.
     * @param {string} path 
     * @param {string} [loginKey=''] 
     * @constructor
     * @memberof PipeClient
     */
    constructor(path, loginKey = '') {
        this._logPrefix = '[PipeClient]'
        this._socket = new net.Socket()

        this._socket.setEncoding('utf-8')
        this._socket.on('error', args =>
            this.defaultEventHandler('ERROR', args)
        )
        this._socket.on('data', args =>
            this.defaultEventHandler('dataRecived', args)
        )
        this._socket.on('end', args =>
            this.defaultEventHandler('end sock', args)
        )
        this._socket.on('close', args =>
            this.defaultEventHandler('close', args)
        )
        this._socket.connect(path, () => {
            this._socket.write(loginKey)
        })
    }

    /**
     * 
     * 
     * @param {any} str 
     * @memberof PipeClient
     */
    sendMessage(str) {
        let message = {
            type: 'message',
            data: str
        }
        let deb = JSON.stringify(message)
        console.log(deb)
        console.log(JSON.parse(deb))
        this._socket.write(JSON.stringify(message))
    }

    /**
     * @description Close daemon process
     * @return {void}
     * @memberof PipeClient
     */
    closeDaemon() {
        let message = {
            type: 'exit',
            data: ''
        }
        this._socket.write(JSON.stringify(message))
    }

    /**
     * @description Default event handler
     * @param {any} eventName 
     * @param {any} data 
     * @memberof PipeClient
     */
    defaultEventHandler(eventName, data) {
        console.log(this._logPrefix, eventName, data)
    }
}

module.exports = PipeClient

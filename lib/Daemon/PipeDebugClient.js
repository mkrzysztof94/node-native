const loginKeys = require('./LoginKeys')
const PipeClient = require('./PipeClient')

/**
 * @namespace Daemon
 * @module Daemon.PipeDebugClient
 * @class PipeDebugClient
 * @extends {PipeClient}
 */
class PipeDebugClient extends PipeClient {
    /**
     * Creates an instance of PipeDebugClient.
     * @param {string} path 
     * @param {string} [loginKey=loginKeys.debug] 
     * @constructor
     * @memberof PipeDebugClient
     */
    constructor(path, loginKey = loginKeys.debug) {
        super(path, loginKey)
        this._logPrefix = 'PipeDebugClient'
    }
}

module.exports = PipeDebugClient

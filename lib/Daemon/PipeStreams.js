const stream = require('stream')

/**
 * @namespace Daemon
 * @module Daemon.PipeStreams
 * @class PipeStreams
 */
class PipeStreams {
    /**
     * Creates an instance of PipeStreams.
     * @param {stream.Readable} inputStream 
     * @param {stream.Writable} outputStream 
     * @param {stream.Writable} errorStream
     * @constructor
     * @memberof PipeStreams
     */
    constructor(inputStream, outputStream, errorStream) {
        if (
            !(inputStream instanceof stream.Readable) ||
            !(outputStream instanceof stream.Writable) ||
            !(errorStream instanceof stream.Writable)
        ) {
            throw Error('Wrong streams injected to constructor')
        }
        this._input = inputStream
        this._output = outputStream
        this._error = errorStream
    }
}

module.exports = PipeStreams

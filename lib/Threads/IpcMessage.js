const BaseClass = require('../BaseClass')
/**
 * @namespace MultiThreads
 * @module MultiThreads.IpcMessage
 * @class IpcMessage
 * @extends {BaseClass}
 */
class IpcMessage extends BaseClass {
    /**
     * Creates an instance of IpcMessage.
     * @param {Object} [msg={ eval: '', log: '' }] 
     * @memberof IpcMessage
     */
    constructor(msg = { eval: '', log: '' }) {
        super()
        this.log = ''
        this.eval = ''
        this.msg = msg
        this._toRun = []
    }

    /**
     * 
     * 
     * @returns  
     * @memberof IpcMessage
     */
    parseEvals() {
        if (typeof this.msg == 'object') {
            if (typeof this.msg.eval === 'string') {
                this.addToEval(this.msg.eval)
            }
        }
        return this
    }

    /**
     * 
     * 
     * @returns  
     * @memberof IpcMessage
     */
    parseLogs() {
        if (typeof this.msg.log === 'string') {
            this.addToLog(this.msg.log)
        }
        return this
    }

    /**
     * 
     * @returns {Promise}
     * @memberof IpcMessage
     */
    async run() {
        for (let i = 0; i < this._toRun.length; i++) {
            this._toRun[i]()
        }
    }

    /**
     * 
     * 
     * @param {any} log 
     * @returns  
     * @memberof IpcMessage
     */
    addLog(log) {
        this.log += ' ' + log
        return this
    }

    /**
     * 
     * 
     * @param {any} evalStr 
     * @returns  
     * @memberof IpcMessage
     */
    addEval(evalStr) {
        this.eval += ' ' + evalStr
        return this
    }

    /**
     * 
     * 
     * @param {any} logStr 
     * @memberof IpcMessage
     */
    addToLog(logStr) {
        this._toRun.push(() => {
            console.log(logStr)
        })
    }

    /**
     * 
     * 
     * @param {any} evalStr 
     * @memberof IpcMessage
     */
    addToEval(evalStr) {
        this._toRun.push(() => setTimeout(eval.bind(null, evalStr), 0))
    }

    /**
     * 
     * 
     * @memberof IpcMessage
     */
    get toRun() {
        return this._toRun
    }

    /**
     * 
     * 
     * @memberof IpcMessage
     */
    set toRun(item) {
        this._toRun.push(item)
    }

    /**
     * 
     * 
     * @static
     * @param {any} msg 
     * @memberof IpcMessage
     */
    static parse(msg) {
        let execMsg = arg => {
            new IpcMessage(arg)
                .parseEvals()
                .parseLogs()
                .run()
        }
        try {
            let inc = JSON.parse(msg[0])
            if (inc instanceof Array) {
                for (let i = 0; i < inc.length; i++) {
                    execMsg(inc[i])
                }
            } else if (typeof inc === 'object') {
                execMsg(inc)
            }
        } catch (err) {
            console.warn(err)
        }
    }

    /**
     * 
     * 
     * @static
     * @returns  
     * @memberof IpcMessage
     */
    static getTemplate() {
        return new IpcMessage()
    }

    /**
     * 
     * 
     * @returns  
     * @memberof IpcMessage
     */
    toSend() {
        return `{ \"eval\": \"${this.eval}\", \"log\": \"${this.log}\" }`
    }
}

module.exports = IpcMessage

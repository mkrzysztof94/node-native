const numCPUs = require('os').cpus().length
const fs = require('fs')
const BaseClass = require('../BaseClass')
const childProcess = require('child_process')
const Worker = require('./Worker')
class MultiThreads extends BaseClass {
    constructor() {
        super()
        this.WCon = []
        this.numCPUs = require('os').cpus().length
        this.Master = process
        this.logOutStream = fs.createWriteStream('./tmp/Worker_out.log')
        this._workerCount = 0
    }

    /**
     * 
     * @returns {Promise}
     * @memberof MultiThreads
     */
    async addWorker() {
        this['Worker_' + this._workerCount] = new Worker(this._workerCount, this.logOutStream)
        this._workerCount++
    }
}

module.exports = MultiThreads

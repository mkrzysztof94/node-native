const BaseClass = require('../BaseClass')
const fs = require('fs')
const childProcess = require('child_process')
let WorkerNumber = 0
const IpcMsg = require('./IpcMessage')

class Worker extends BaseClass {
    constructor(forkedProcessNumber, logOutFile, options = {}) {
        options = Object.assign(Worker.getDefaultOptions(), options)
        super()
        this.options = options
        this.logOutFile = logOutFile
        this.process = childProcess.fork(
            './src/MultiThreads/WorkerRun.js',
            [
                `Worker_${forkedProcessNumber} `
                //  id
            ],
            {
                cwd: process.cwd(),
                env: process.env,
                execPath: process.execPath,
                silent: false,
                stdio: ['pipe', 'pipe', 'pipe', 'ipc']
            }
        )
        this.preFix = `[Worker:${this.options.name}]`
        this.process.on('message', (...xxx) => this.onMessage(xxx), this.errorHandler)
        this.process.on('exit', (...xxx) => this.onExit(xxx))
        WorkerNumber++
    }

    /**
     * 
     * @returns {Promise}
     * @param {any} xxx 
     * @memberof Worker
     */
    async send(xxx) {
        this.process.send(xxx.toString(), () => console.log(`${this.preFix} <<== ${xxx.toString()} `))
    }

    /**
     * 
     * @returns {Promise}
     * @param {any} message 
     * @memberof Worker
     */
    async onMessage(...message) {
        let msg = `${this.preFix} ${this.process.pid} Message recived: ${JSON.stringify(message)}`
        console.info(msg)
        this.logOutFile.write(msg)
        IpcMsg.parse(message[0])
    }

    /**
     * 
     * 
     * @param {any} args 
     * @returns {Promise}
     * @memberof Worker
     */
    async onExit(...args) {
        let msg = `${this.preFix} exited ${JSON.stringify(args)} `
        console.warn(msg)
        this.logOutFile.write(msg)
    }

    /**
     * 
     * @param {any} error 
     * @param {any} args 
     * @returns {Promise}
     * @memberof Worker
     */
    async ErrorHandle(error, ...args) {
        let msg = `${this.preFix} => ErrorHandle : ${JSON.stringify(error)} ${JSON.stringify(args)}`
        console.log(msg)
        this.logOutFile.write(msg)
    }
    /**
     * 
     * 
     * @param {any} error 
     * @returns {Promise}
     * @memberof Worker
     */
    async errorHandler(error) {
        if (error !== null) console.error(`Error during send to process !! ${error}`)
        else console.info('Message sended.')
    }

    static getDefaultOptions() {
        return {
            env: process.env,
            name: `Worker_${WorkerNumber}`,
            outFile: `./tmp_${this.name}_out.log`
        }
    }
}

module.exports = Worker

var port = 9292
var openInspector = false

const inspector = require('inspector')
const fs = require('fs')
const net = require('net')
const path = require('path')
const IpcMsg = require('./IpcMessage')
const sockPath = path.join('\\\\?\\pipe', process.cwd(), 'myctl')

module.exports = path => {
    const errorHandler = error => {
        if (error !== null)
            console.error(`Error during send to process !! ${error}`)
        else console.info('Message sended.')
    }

    var runLoopTimeout = () => {
        setTimeout(runLoopTimeout, 600000)
    }
    var runLoop = setTimeout(runLoopTimeout, 600)

    process.id = ''
    if (process.argv.length >= 3) {
        process.id = process.argv[2]
    }

    let messageWakeup = `Worker ${process.id} ${process.pid} started`
    const JestemWorkerem = true

    if (process.argv.length >= 4) {
        messageWakeup += ` Inspector waiting on localhost:${port} `
        port += parseInt(process.argv[3])
        openInspector = true
    }

    process
        .on('message', (message, error) => {
            let msg = `[Master] ==>> ${message}`
            console.log(msg)
            IpcMsg.parse(message)
        })
        .on('disconnect', message => {
            console.log('Disconnecting !!!')
            process.disconnect()
            process.exit(0)
        })
        .send(
            IpcMsg.getTemplate()
                .addLog(messageWakeup)
                .toSend(),
            errorHandler
        )

    if (openInspector) {
        inspector.open(port, 'localhost', true)
    }
}

/*
const openedInstructor = inspector.open(9292, "localhost", true);
*/

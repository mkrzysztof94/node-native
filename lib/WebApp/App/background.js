var source = new EventSource('http://localhost:89')
var mainWindow = null

chrome.app.runtime.onLaunched.addListener(function() {
    mainWindow = chrome.app.window.create('mainWindow.html', {
        outerBounds: {
            width: 400,
            height: 500
        }
    })
})

source.onmessage = function(event) {
    console.log(event)
    mainWindow.document.getElementById('EventsEmited').innerHTML += event.data
}

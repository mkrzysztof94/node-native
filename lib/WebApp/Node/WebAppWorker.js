/*
var source = new EventSource("demo_sse.php");
source.onmessage = function(event) {
    document.getElementById("result").innerHTML += event.data + "<br>";
}; 
*/
const http = require('http')
const net = require('net')
const stream = require('stream')
const fs = require('fs')
class WebAppWorker {
    constructor() {
        this.eventId = 0
        this.bytesWrittenAndReaden = 0
        this._inp = fs.createReadStream('./rss', {
            encoding: 'utf-8',
            flags: 'w+'
        })
        this._out = fs.createWriteStream('./rss', {
            encoding: 'utf-8',
            flags: 'w+'
        })

        this._inp.on('error', error => {
            console.log(error)
        })
        this._out.on('data', data => {
            this._out.write(data, error => console.log(error))
        })
        this._out.on('error', error => {
            console.log(error)
        })
        var sock = new net.Socket()
        this.res = null
        this.eventServer = http.createServer((req, resp) => {
            // req.destroy()
            //req.resume()
            resp.setHeader('Content-Type', 'text/event-stream')
            resp.setHeader('Cache-Control', 'no-cache')
            resp.setHeader('Access-Control-Allow-Origin', '*')
            resp.setHeader('Transfer-Encoding', 'deflate')
            // resp.setHeader('Transfer-Encoding', 'utf-8')
            resp.on('error', err => console.log(err))
            resp.write(this.flushRead())
            this.res = resp
        })
    }

    flushRead() {
        let data = new Date(Date.now())
        let out =
            //'\n\nevent:update\n' +
            // `id:${this.eventId}\n` +
            `data:[${data.toLocaleDateString()}^${data.toLocaleTimeString()}]\n\n`
        this.eventId++
        return out
    }
    run() {
        this.eventServer.listen(89, 'localhost')
    }
}
module.exports = WebAppWorker

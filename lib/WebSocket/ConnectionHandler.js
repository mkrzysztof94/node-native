const BaseClass = require('../BaseClass')
const Utilities = require('./Utilities')
const events = require('events')
const net = require('net')

/**
 * @module ConnectionHandler
 * @class ConnectionHandler
 * @extends {BaseClass}
 */
class ConnectionHandler extends BaseClass {
    /**
     * Creates an instance of ConnectionHandler.
     * @param {net.Socket} sock
     * @param {Utilities} utils
     * @constructor
     * @memberof WebSocketConnection
     */
    constructor(sock, utils) {
        if (!(utils instanceof Utilities)) {
            throw Error('Utils should be instance of Utilities')
        }
        if (!(sock instanceof net.Socket)) {
            throw Error('Utils should be instance of Utilities')
        }
        super()
        this.utils = utils
        this._sock = sock
        this._sock.on('error', err => this.errorHandler(args))
        this._sock.on('close', args => this.closeHandler(args))
        this._sock.on('data', data => this.incomingRequest(data))
        this._sock.on(this.utils.OPCodes[1], (...args) => this.messageHandler(...args))
        this._sock.on(this.utils.OPCodes[9], () => this.sendPong())
        this._sock.on(this.utils.OPCodes[10], () => this.sendPing())
        this._sock.setKeepAlive(true, 1000)
    }

    /**
     * @event Ping
     * @returns {Promise}
     * @memberof ConnectionHandler
     */
    async sendPing() {
        this._sock.write(this.utils.Ping)
    }

    /**
     * @event Pong
     * @returns {Promise}
     * @memberof ConnectionHandler
     */
    async sendPong() {
        this._sock.write(this.utils.Pong)
    }

    /**
     * @event Close
     * @returns {Promise}
     * @memberof ConnectionHandler
     */
    async sendClose() {
        this._sock.write(this.utils.Close)
    }

    /**
     * @param {Error} err
     * @event error
     * @returns {Promise} 
     * @memberof ConnectionHandler
     */
    async errorHandler(err) {
        console.log(err)
    }

    /**
     * @param {String} err
     * @event close
     * @returns {Promise}
     * @memberof ConnectionHandler
     */
    async closeHandler(msg) {
        console.log(msg)
    }

    /**
     * @event data
     * @param {Buffer} msg
     * @param {Socket} autorSocket
     * @returns {Promise}
     * @memberof WebSocketServer
     */
    async incomingRequest(data) {
        let OPCode = data[0] & 0x0f
        if (this.utils.OPCodes[OPCode] !== undefined) {
            this._sock.emit(this.utils.OPCodes[OPCode], data)
        }
    }

    /**
     * @param {Buffer} msg 
     * @returns {Promise}
     * @memberof ConnectionHandler
     */
    async messageHandler(msg) {
        let mInfo = this.utils.createFrameInfo(msg)
        let data = null
        console.log('Final: ', mInfo.isFinal)
        if (mInfo.isMasked) {
            mInfo.mask = msg.subarray(mInfo.dataOffSet, mInfo.dataOffSet + 4)
            mInfo.dataOffSet += 4
            data = this.utils.unmaskData(mInfo.mask, msg.subarray(mInfo.dataOffSet))
        } else {
            data = msg.subarray(mInfo.dataOffSet).toString()
        }

        console.log(data)
    }
}

module.exports = ConnectionHandler

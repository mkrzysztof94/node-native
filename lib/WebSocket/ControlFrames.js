const BaseClass = require('../BaseClass')
const PrivatesSymbols = {
    pong: Symbol('pong'),
    ping: Symbol('ping'),
    close: Symbol('close'),
    OPCodes: Symbol('opcodes')
}

/**
 * @class ControlFrames
 * @extends BaseClass
 */
class ControlFrames extends BaseClass {
    /**
     * Creates an instance of ControlFrames.
     * @memberof ControlFrames
     */
    constructor() {
        super()
        this[PrivatesSymbols.pong] = ControlFrames.buildEmptyPongFrame()
        this[PrivatesSymbols.ping] = ControlFrames.buildEmptyPingFrame()
        this[PrivatesSymbols.close] = ControlFrames.buildEmptyCloseFrame()
        this[PrivatesSymbols.OPCodes] = {
            0: 'ContinuationFrame',
            1: 'TextFrame',
            2: 'BinaryFrame',
            8: 'ConnectionCloseFrame',
            9: 'PingFrame',
            10: 'PongFrame'
        }
    }

    /**
     * @readonly
     * @memberof ControlFrames
     */
    get OPCodes() {
        return this[PrivatesSymbols.OPCodes]
    }

    /**
     * @description Returns new Buffer with unmasked Ping frame
     * @static
     * @returns  {Buffer}
     * @memberof ControlFrames
     */
    static buildEmptyPingFrame() {
        return Buffer.from([0x89, 0 & 0x7f])
    }

    /**
     * @description Returns Buffer with unmasked Ping frame
     * @property {Ping}
     * @returns  {Buffer}
     * @memberof ControlFrames
     */
    get Ping() {
        return this[PrivatesSymbols.ping]
    }

    set Ping(val) {}

    /**
     * @description Returns new Buffer with unmasked Pong frame
     * @static
     * @returns  {Buffer}
     * @memberof ControlFrames
     */
    static buildEmptyPongFrame() {
        return Buffer.from([0x8a, 0 & 0x7f])
    }

    /**
     * @description Returns Buffer with unmasked Pong frame
     * @property {Pong}
     * @returns  {Buffer}
     * @memberof ControlFrames
     */
    get Pong() {
        return this[PrivatesSymbols.pong]
    }

    set Pong(val) {}

    /**
     * @description Returns new Buffer with unmasked Ping frame
     * 
     * @static
     * @returns {Buffer}  
     * @memberof ControlFrames
     */
    static buildEmptyCloseFrame() {
        return Buffer.from([0x88, 0 & 0x7f])
    }

    /**
     * @description Returns Buffer with unmasked Pong frame
     * @property {Close}
     * @returns  {Buffer}
     * @memberof ControlFrames
     */
    get Close() {
        return this[PrivatesSymbols.close]
    }

    set Close(val) {}
}

module.exports = ControlFrames

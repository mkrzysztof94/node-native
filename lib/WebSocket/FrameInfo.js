const BaseClass = require('../BaseClass')
const prv = {
    finalBit: Symbol('finalBit'),
    maskBit: Symbol('maskBit'),
    dataOffSet: Symbol('dataOffSet'),
    size: Symbol('size'),
    sizeOpt: Symbol('sizeOpt')
}

/**
 * @class FrameInfo
 * @extends {BaseClass}
 */
class FrameInfo extends BaseClass {
    /**
     * Creates an instance of FrameInfo.
     * @param {Buffer} msg
     * @memberof FrameInfo
     */
    constructor(msg) {
        super()
        this[prv.finalBit] = msg[0] >> 7
        this[prv.maskBit] = msg[1] >> 7
        this[prv.dataOffSet] = 2
        this[prv.size] = null
        this[prv.sizeOpt] = null
        this.mask = null
        this.constructMsgSizeInfo(msg)
    }

    /**
     * 
     * @readonly
     * @returns {Boolean}
     * @memberof FrameInfo
     */
    get isFinal() {
        return Boolean(this[prv.finalBit])
    }

    /**
     * 
     * 
     * @readonly
     * @memberof FrameInfo
     */
    get isMasked() {
        return Boolean(this[prv.maskBit])
    }

    /**
     * @property {Number} dataOffSet
     * @returns {Number}
     * @memberof FrameInfo
     */
    get dataOffSet() {
        return this[prv.dataOffSet]
    }
    set dataOffSet(val) {
        this[prv.dataOffSet] = val
        return this[prv.dataOffSet]
    }

    get size() {
        return this[prv.size]
    }
    set size(val) {
        this[prv.size] = val
        return this[prv.size]
    }

    get sizeOpt() {
        return this[prv.sizeOpt]
    }
    set sizeOpt(val) {
        this[prv.sizeOpt] = val
        return this[prv.sizeOpt]
    }

    /**
     * @description getMessageLength
     * @param {Buffer} msg
     * @returns {void}
     * @memberof FrameInfo
     */
    constructMsgSizeInfo(msg) {
        let msgSize = msg[1] & 0x7f
        if (0 <= msgSize && msgSize <= 125) {
            this.size = msgSize
            this.sizeOpt = 0
        } else if (msgSize === 126) {
            this.dataOffSet += 2
            this.size = (msg[2] << 8) | msg[3]
            this.sizeOpt = 1
        } else if (msgSize === 127) {
            this.dataOffSet += 4
            this.size =
                (msg[2] << 56) |
                (msg[3] << 48) |
                (msg[4] << 40) |
                (msg[5] << 32) |
                (msg[6] << 24) |
                (msg[7] << 16) |
                (msg[8] << 8) |
                msg[9]
            this.sizeOpt = 2
        } else {
            console.log('error kurwa error')
            this.sizeOpt = 3
        }
    }
}

module.exports = FrameInfo

const https = require('https')
const http = require('http')
const net = require('net')
const tls = require('tls')
const crypto = require('crypto')
const fs = require('fs')
const path = require('path')
const Utilities = require('./Utilities')
const ConnectionHandler = require('./ConnectionHandler')
console.log(this)
const options = {
    // key: fs.readFileSync('./keys/key.pem'),
    // cert: fs.readFileSync('/keys/cert.pem'),
    SNICallback: (...args) => {
        console.log('SNICAll', args)
        console.log('okSnicall', args[1])
    },
    handshakeTimeout: 99999,
    secureProtocol: true,
    rejectUnauthorized: false
}

/**
 * @description 
 * @class Server
 * @extends {http.Server}
 */
class Server extends http.Server {
    /**
     * @description Creates an instance of Server.
     * @constructor
     * @memberof Server
     */
    constructor() {
        super(args => console.log('createServer', args))
        this.utils = new Utilities()
        this.on('upgrade', (...args) => this.upgradeHandler(...args))
    }

    /**
     * @param {Number} [port=80] 
     * @param {String} [host='localhost'] 
     * @memberof Server
     */
    start(port = 80, host = 'localhost') {
        this.listen(port, 'localhost', (...args) => {
            console.log('Server listing !', args, this.address())
        })
    }

    /**
     * @description Handler for upgradeEvent
     * @event onUpgrade
     * @param {http.IncomingMessage} msgInp 
     * @param {net.Socket} socket 
     * @param {Buffer} buffer 
     * @returns {Boolean}
     * @memberof Server
     */
    upgradeHandler(msgInp, socket, buffer) {
        if (typeof msgInp.headers['sec-websocket-key'] === 'undefined') {
            return socket.end()
        }
        this.tmp = new ConnectionHandler(socket.ref(), this.utils)

        return socket.write(this.utils.creatAuthResponse(msgInp))
    }
}

module.exports = Server

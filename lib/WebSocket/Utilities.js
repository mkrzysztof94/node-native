const crypto = require('crypto')
const ControlFrames = require('./ControlFrames')
const FrameInfo = require('./FrameInfo')

/**
 * @module Utilities
 * @class Utilities
 */
class Utilities extends ControlFrames {
    constructor() {
        super()
    }

    /**
    * @param {[]} mask 
    * @param {[]} data 
    * @return {String}  
    * @memberof Utilities
    */
    unmaskData(mask, data) {
        let out = String('')
        for (let i = 0; i < data.length; i++) {
            out += String.fromCharCode(data[i] ^ mask[i % 4])
        }
        return out
    }

    /**
     * @description Creates FrameInfo
     * 
     * @param {Buffer} msg 
     * @returns  {FrameInfo}
     * @memberof Utilities
     */
    createFrameInfo(msg) {
        return new FrameInfo(msg)
    }

    /**
     * @description Returns webSocket handshake header
     * 
     * @param {http.IncomingMessage} request 
     * @returns {String}
     * @memberof Utilities
     */
    creatAuthResponse(request) {
        let key = this.getHandShakeKey(request.headers['sec-websocket-key'])
        let handShakeHeaders = [
            'HTTP/1.1 101 Switching Protocols',
            'Connection: keep-alive, Upgrade',
            'Upgrade: websocket',
            'Origin: localhost',
            'Host: localhost',
            `Sec-WebSocket-Accept: ${key}`,
            // 'Sec-WebSocket-Version: 13',
            // 'Sec-WebSocket-Extensions: \r\n',
            'cache-control: no-cache',
            '\r\n'
        ]
        return handShakeHeaders.join('\r\n')
    }

    /**
     * @description Generates key for webSocket handshake
     * @param {String} key 
     * @returns  {String}
     * @memberof Utilities
     */
    getHandShakeKey(key) {
        return crypto
            .createHash('sha1')
            .update(`${key}258EAFA5-E914-47DA-95CA-C5AB0DC85B11`)
            .digest('base64')
    }
}

module.exports = Utilities

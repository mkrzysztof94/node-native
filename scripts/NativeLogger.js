const runType = (process.argv.length == 3 &&  process.argv[2] == 'Debug' ? 'Debug' : 'Release')
const NativePack = require(`${__dirname}/../src/build/${runType}/Logger`)
const repl = require('repl')
console.log('Logger process: ',process.pid)
const Logger = NativePack.Logger
let logger = new Logger('NativeLogger', '/media/adi/Nowy/nativ-node/NativNode/tmp/DebugLogger.txt')

let srep = repl.start({ prompt: '$==>' })

srep.context.Logger = Logger
srep.context.logger = logger
srep.context.NativePack = NativePack


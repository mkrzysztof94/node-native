const runType = (process.argv.length == 3 &&  process.argv[2] == 'Debug' ? 'Debug' : 'Release')
const NativePack = require(`${__dirname}/../src/build/${runType}/Pipe`)
const repl = require('repl')
console.log('Pipe client process: ',process.pid)
const Pipe = NativePack.Pipe
let pipeClient = new Pipe.PipeClient()



let srep = repl.start({ prompt: '$==>' })

srep.context.pipeClient = pipeClient
srep.context.Pipe = Pipe
srep.context.NativePack = NativePack

srep.context.fake = setTimeout(() => {
    pipeClient.Connect()
    pipeClient.Send('Welcome\n')
    pipeClient.Send('Welcome\n')
    pipeClient.Send('Welcome\n')
    pipeClient.Send('Welcome\n')
    clearTimeout(srep.context.fake)
}, 2000)
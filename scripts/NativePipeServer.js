const runType = (process.argv.length == 3 &&  process.argv[2] == 'Debug' ? 'Debug' : 'Release')
const NativePack = require(`${__dirname}/../src/build/${runType}/Pipe`)
const repl = require('repl')
const fs = require('fs')
const childPro = require('child_process')

if (fs.existsSync('/media/adi/Nowy/nativ-node/NativNode/tmp/pipe.sock') === true) {
    fs.unlinkSync('/media/adi/Nowy/nativ-node/NativNode/tmp/pipe.sock')
}
console.log('Pipe server process: ', process.pid)
const Pipe = NativePack.Pipe
let pipeServer = new Pipe.PipeServer()

let srep = repl.start({ prompt: '$==>' })

srep.context.pipeServer = pipeServer
srep.context.Pipe = Pipe
srep.context.NativePack = NativePack
pipeServer.Open()
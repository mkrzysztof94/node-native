const runType = (process.argv.length == 3 &&  process.argv[2] == 'Debug' ? 'Debug' : 'Release')
const NativePack = require(`${__dirname}/../src/build/${runType}/NativePack`)
const Terminal = NativePack.Terminal
let terminal = new Terminal()

console.log(process.pid)
console.log(Terminal)
console.log(runType)
console.log(terminal)

terminal.ClearScreen()
terminal.Log('Log test\n')
terminal.Warn('Warn test\n')
terminal.Error('Error test\n')
terminal.Write(`Welcome to terminal Width: ${terminal.GetWidth()} Height: ${terminal.GetHeight()}\n`)

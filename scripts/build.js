const path = require('path')
const cp = require('child_process')
const exec = cp.execFileSync
process.chdir(path.resolve(__dirname + '/../'))
const cwd = process.cwd()

cp.execSync('node-gyp --silly -j 4 -C ./src/ configure build', {
    cwd: cwd,
    stdio: 'inherit',
    // windowsHide: 'false',
    // shell: true,
    encoding: 'utf8'
})

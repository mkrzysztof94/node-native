const inspector = require('inspector')
const clus = require('cluster')
const fs = require('fs')
const net = require('net')
const path = require('path')
const stream = require('stream')
const event = require('events')
tty = require('tty')
Super = require('./src/lib').Super
native = require('./src/index')
cluster = clus
IpcMsg = require('./src/MultiThreads/IpcMessage')
const { MultiThreads } = require('./src/MultiThreads/')
sockPath = path.join('\\\\?\\pipe', process.cwd(), 'myctl')

openedInstructor = inspector.open(9192, 'localhost', true)
self = this
multi = new MultiThreads(sockPath)

console.log('Master PID !', process.pid)
nativeObj = new native.NativPack()
var readL = require('readline')
var add = () => {
    setTimeout(add, 600000)
}
//inp = process.openStdin()
defaultLog = (operation, ...xxx) => {
    console.log(`[Pipe:${operation}] ==>`, xxx)
}
Pipe = new stream.Duplex({
    allowHalfOpen: true,
    readableObjectMode: true,
    writableObjectMode: true,
    write: (...args) => defaultLog('WRITE', args),
    writev: (...args) => defaultLog('WRITEV', args),
    destroy: (...args) => defaultLog('DESTROY', args),
    final: (...args) => defaultLog('FINAL', args),
    read: (...args) => defaultLog('READ', args),
    encoding: 'utf8',
    objectMode: true,
    decodeStrings: true,
    highWaterMark: 16384
})
const signals = [
    // 'SIGABRT',
    // 'SIGALRM',
    // 'SIGBUS',
    // 'SIGCHLD',
    // 'SIGCONT',
    // 'SIGFPE',
    // 'SIGHUP',
    // 'SIGILL',
    // 'SIGINT',
    // 'SIGIO',
    // 'SIGIOT',
    // 'SIGKILL',
    // 'SIGPIPE',
    // 'SIGPOLL',
    // 'SIGPROF',
    // 'SIGPWR',
    // 'SIGQUIT',
    // 'SIGSEGV',
    // 'SIGSTKFLT',
    // 'SIGSTOP',
    // 'SIGSYS',
    // 'SIGTERM',
    // 'SIGTRAP',
    // 'SIGTSTP',
    // 'SIGTTIN',
    // 'SIGTTOU',
    // 'SIGUNUSED',
    // 'SIGURG',
    // 'SIGUSR1',
    // 'SIGUSR2',
    // 'SIGVTALRM',
    // 'SIGWINCH',
    // 'SIGXCPU',
    // 'SIGXFSZ',
    // 'SIGBREAK',
    // 'SIGLOST',
    // 'SIGINFO',
    'error',
    // 'line',
    // 'readline',
    // 'input',
    'data',
    'pause',
    'resume',
    'line',
    'close',
    'SIGCONT',
    'SIGINT',
    'SIGTSTP',
    'chunk',
    'read',
    'keypress'
    // 'key'
]

Pipe.on('data', (...args) => defaultLog('onDATA', args))
Pipe.on('chunk', (...args) => defaultLog('onCHUNK', args))
Pipe.on('message', (...args) => defaultLog('onMESSAGE', args))
setTimeout(add, 600)
process.on('SIGINT', () => {
    console.log('Got a SIGINT. Goodbye cruel world')
    process.exit(0)
})

uiInst = new native.UI()
const context = vm.createContext()
uiInst.run(context)
// uiInst.run() //

// for (let i = 0; i < signals.length; i++) {
//     process.stdin.on(
//         signals[i],
//         (...args) => defaultLog('IN:' + signals[i], args),
//         (...args) => defaultLog('IN2:' + signals[i], args)
//     )
// }

// process.stdout.on('resize', () => {
//     console.log('screen size has changed!')
//     console.log(`${process.stdout.columns}x${process.stdout.rows}`)
// })

// outS = process.stdout.resume()
// process.stdin.on('data', (...xxx) => {
//     console.log('Master recived !', JSON.stringify(xxx))
//     //process.send("Master recived !", xxx)
// })
/*
process.stdin.on("message", (...xxx)=>{
    console.log("Master recived !", JSON.stringify(xxx))
    //process.send("Master recived !", xxx)
})

process.on("message", (...xxx)=>{
    console.log("Master recived !", JSON.stringify(xxx))
    //process.send("Master recived !", xxx)
})
process.on("data", (...xxx)=>{
    console.log("Master recived !", JSON.stringify(xxx))
    //process.send("Master recived !", xxx)
})
*/

const path = require('path')
const packageDir = path.resolve(`${__dirname}/../`)

const DaemonProcess = require('../lib/Daemon/DaemonProcess')
const inspector = require('inspector')

if (typeof process.env.__daemon !== 'undefined') {
    setTimeout(() => DaemonProcess.daemonLoop(), 1000)
    inspector.open(9192, 'localhost', false)
    // sto = require()
}
daemon = DaemonProcess.Spawn(`${__dirname}/`)

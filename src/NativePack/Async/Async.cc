#include "Includes.h"
#include "Async.h"

namespace NativePack
{
    void Async::AsyncWorkCall(
        v8::FunctionCallback toCall, WorkAsyncPtr workAsyncPtr, WorkAsyncCompletePtr workAsyncCompletePtr)
    {
        Isolate*        iso = Isolate::GetCurrent();
        v8::HandleScope scope(iso);
        Work*           work = new Work();
        work->request.data   = work;
        work->callback.Reset(iso, Function::New(iso, toCall));
        uv_queue_work(uv_default_loop(), &work->request, workAsyncPtr, workAsyncCompletePtr);
    }

    void Async::WorkAsync(uv_work_t* req)
    {
    }

    void Async::WorkAsyncComplete(uv_work_t* req, int status)
    {
        Work*       work    = static_cast<Work*>(req->data);
        Isolate*    isolate = Isolate::GetCurrent();
        HandleScope handleScope(isolate);

        v8::Local<Value> argv[] = {v8::Null(isolate)};

        work->callback.Get(isolate)->Call(isolate->GetCurrentContext()->Global(), 2, argv);
        delete work;
    }
} // namespace NativePack
#include "Includes.h"
#include "AsyncCallback.h"

namespace NativePack
{
    void AsyncCallback::callAsyncHandler(uv_async_t* handle)
    {
        AsyncCallback* aMsg = static_cast<AsyncCallback*>(handle->data);
        Isolate*       iso  = Isolate::GetCurrent();
        HandleScope    scope(iso);
        Local<Value>   argv[1];
        argv[0] = aMsg->completeCallback.Get(iso);
        if (aMsg->isLoop)
        {
            uv_async_send(aMsg->handle);
        }
        else
        {
            uv_close((uv_handle_t*) handle, NULL);
            delete aMsg;
        }
    }

    AsyncCallback::AsyncCallback(
        Local<Function> callBack, Local<Function> completeCb, bool isLoop, callAsyncHandlerPtr callAsyncHandler)
    {
        Isolate* iso       = Isolate::GetCurrent();
        this->handle       = (uv_async_t*) malloc(sizeof(uv_async_t));
        this->handle->data = this;
        this->callback.Reset(iso, callBack);
        this->completeCallback.Reset(iso, completeCb);
        this->isLoop = isLoop;
        uv_async_init(uv_default_loop(), this->handle, callAsyncHandler);
        uv_async_send(this->handle);
    }

    void AsyncCallback::Init(Local<Function> callBack, Local<Function> completeCb, bool isLoop)
    {
        new AsyncCallback(callBack, completeCb, isLoop);
    }
} // namespace NativePack
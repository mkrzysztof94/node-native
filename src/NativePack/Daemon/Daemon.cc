#include "Includes.h"
#include "Daemon.h"
#include "Utils.h"

namespace NativePack
{
    Local<Function> Daemon::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*                isolate = exports->GetIsolate();
        Local<FunctionTemplate> templ   = Daemon::createClass(isolate);
        /*
                Daemon::AddWrapMethods(
                        node::Environment::GetCurrent(isolate),
                        templ
                );*/
        return templ->GetFunction();
    }

    Local<FunctionTemplate> Daemon::createClass(Isolate* isolate)
    {
        return Utils::JSClassBuilder::Create("Daemon", Daemon::New)
            ->AddFunction("start", Daemon::Start)
            ->AddFunction("con", Daemon::Connect)
            ->BuildClass(isolate);
    }

    void Daemon::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that = args.This();
        Daemon*       dae  = new Daemon(that, args.GetIsolate());
        args.GetReturnValue().Set(*dae->that);
    }

    void Daemon::Callback(uv_poll_t* handle, int status, int events)
    { /*
std::cout << "CALLBACK";
HandleScope handle_scope(Isolate::GetCurrent());
Daemon* wrap = static_cast<Daemon*>(handle->data);
node::Environment* env = node::Environment::GetCurrent(Isolate::GetCurrent());
Context::Scope context_scope(env->context());
Local<Value> arg = Integer::New(env->isolate(), status);
std::cout << env->onchange_string()->GetExternalStringResource();
*/
    }

    void Daemon::Start(FunctionCallbackArg args)
    {
        Isolate*           iso = args.GetIsolate();
        node::Environment* env = node::Environment::GetCurrent(iso);
        HandleScope        scope(iso);
        Daemon*            dae = Daemon::Unwrap(args.This());
        uv_pipe_init(env->event_loop(), dae->handle, 1 /* ipc */);
        uv_pipe_open(dae->handle, 0);

        int r;
        if ((r = uv_pipe_bind(dae->handle, Daemon::pipeAddress())))
        {
            fprintf(stderr, "Bind error %s\n", uv_err_name(r));
        }
        if ((r = uv_listen((uv_stream_t*) dae->handle, 128, Daemon::onNewConnection)))
        {
            fprintf(stderr, "Listen error %s\n", uv_err_name(r));
        }
        /*
      std::cout << std::endl
              << "Name: " << dae->handle->name << std::endl;
      std::cout << std::endl
              << "activecnt: " << dae->handle->activecnt << std::endl;
      std::cout << std::endl
              << uv_pipe_pending_count(dae->handle) << std::endl;
      */
        uv_run(dae->handle->loop, uv_run_mode::UV_RUN_NOWAIT);
    }

    void Daemon::onNewConnection(uv_stream_t* stream, int status)
    {
        std::cout << "New connection ! : " << status << std::endl;

        std::cout << std::endl << stream->write_queue_size << std::endl;
        int nread = status;
        if (nread < 0)
        {
            if (nread != UV_EOF)
                fprintf(stderr, "Read error %s\n", uv_err_name(nread));
            uv_close((uv_handle_t*) stream, NULL);
            return;
        }

        uv_pipe_t* pipe = (uv_pipe_t*) stream;
        if (!uv_pipe_pending_count(pipe))
        {
            fprintf(stderr, "No pending count\n");
            return;
        }

        uv_handle_type pending = uv_pipe_pending_type(pipe);
        assert(pending == UV_TCP);

        uv_tcp_t* client = (uv_tcp_t*) malloc(sizeof(uv_tcp_t));
        uv_tcp_init(uv_default_loop(), client);
        if (uv_accept(stream, (uv_stream_t*) client) == 0)
        {
            uv_os_fd_t fd;
            uv_fileno((const uv_handle_t*) client, &fd);
            // fprintf(stderr, "Worker %d: Accepted fd %d\n", getpid(), fd);
            // uv_read_start((uv_stream_t*)client, alloc_buffer, echo_read);
        }
        else
        {
            uv_close((uv_handle_t*) client, NULL);
        }
    }

    void Daemon::conCb(uv_connect_t* conn, int status)
    {
        std::cout << std::endl << Daemon::pipeAddress() << std::endl;
    }

    void Daemon::Connect(FunctionCallbackArg args)
    {
        Isolate*           iso = args.GetIsolate();
        node::Environment* env = node::Environment::GetCurrent(iso);
        HandleScope        scope(iso);
        Daemon*            dae = Daemon::Unwrap(args.This());
        dae->handleCon->data   = dae;
        uv_pipe_init(env->event_loop(), dae->handle, 1 /* ipc */);
        uv_pipe_connect(dae->handleCon, dae->handle, Daemon::pipeAddress(), Daemon::onPipeConnect);
        uv_run(dae->handle->loop, uv_run_mode::UV_RUN_NOWAIT);
    }

    void Daemon::onPipeConnect(uv_connect_t* req, int status)
    {
        std::cout << "KURWA KONNECTED ";
    }

    Daemon::Daemon(Local<Object> target, Isolate* isolate)
    {
        HandleScope scope(isolate);

        this->that   = &target;
        this->handle = (uv_pipe_t*) malloc(sizeof(uv_pipe_t));
        this->Wrap(target);
        this->Ref();
    }

    Daemon* Daemon::Unwrap(Local<Object> daemonWrap)
    {
        return node::ObjectWrap::Unwrap<Daemon>(daemonWrap);
    }

    int Daemon::Main()
    {
        return 0;
    }
} // namespace NativePack
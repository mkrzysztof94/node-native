#include "Includes.h"
#include "FileWatcher.h"

namespace NativePack
{
    FileWatcher::FileWatcher(Local<Object> target, Isolate* isolate)
    {
        HandleScope scope(isolate);

        this->that   = &target;
        this->handle = new uv_fs_poll_t();
        this->Wrap(target);
        this->Ref();
        this->handle->data = static_cast<void*>(this);
        uv_fs_poll_init(node::Environment::GetCurrent(isolate)->event_loop(), this->handle);
    }

    FileWatcher* FileWatcher::Unwrap(Local<Object> daemonWrap)
    {
        return node::ObjectWrap::Unwrap<FileWatcher>(daemonWrap);
    }
} // namespace NativePack
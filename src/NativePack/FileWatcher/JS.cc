#include "Includes.h"
#include "FileWatcher.h"
#include "Utils.h"

namespace NativePack
{
    using NativePack::FileWatcher;

    Local<Function> FileWatcherJS::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*                isolate = exports->GetIsolate();
        Local<FunctionTemplate> con     = FunctionTemplate::New(isolate, FileWatcherJS::New);
        con->InstanceTemplate()->SetInternalFieldCount(1);
        con->SetClassName(String::NewFromUtf8(isolate, "FileWatcher"));
        con->PrototypeTemplate()->Set(isolate, "start", FunctionTemplate::New(isolate, FileWatcherJS::Start));

        return con->GetFunction();
    }

    void FileWatcherJS::Start(FunctionCallbackArg args)
    {
        HandleScope  scope(args.GetIsolate());
        FileWatcher* dae = FileWatcher::Unwrap(args.This());
        uv_fs_poll_start(dae->handle, FileWatcherJS::Callback, "D:/nativ-node/ssh-connector/src/watch.txt", 3000);
    }

    void FileWatcherJS::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that = args.This();
        FileWatcher*  dae  = new FileWatcher(that, args.GetIsolate());
        args.GetReturnValue().Set(*dae->that);
    }

    void FileWatcherJS::Callback(uv_fs_poll_t* handle, int status, const uv_stat_t* prev, const uv_stat_t* curr)
    {
        std::cout << "CALLBACK";
        HandleScope        handle_scope(Isolate::GetCurrent());
        node::Environment* env = node::Environment::GetCurrent(Isolate::GetCurrent());
        Context::Scope     context_scope(env->context());
        std::cout << env->onchange_string()->GetExternalStringResource();
    }
} // namespace NativePack
#ifndef __NATIVEPACK_MODULE_ASYNC_MODULE_H__
#define __NATIVEPACK_MODULE_ASYNC_MODULE_H__

#include "Includes.h"

namespace NativePack
{
    class Async
    {
      public:
        static void AsyncWorkCall(
            FunctionCallback     toCall,
            WorkAsyncPtr         workAsyncPtr         = Async::WorkAsync,
            WorkAsyncCompletePtr workAsyncCompletePtr = Async::WorkAsyncComplete);
        static void WorkAsync(uv_work_t* req);
        static void WorkAsyncComplete(uv_work_t* req, int status);
    };

    struct Work
    {
        uv_work_t            request;
        Persistent<Function> callback;
        Persistent<Function> completeCallback;
        bool                 canceled = false;
    };
} // namespace NativePack

#endif
#ifndef __NATIVEPACK_MODULE_ASYNC_MESSAGE_MODULE_H__
#define __NATIVEPACK_MODULE_ASYNC_MESSAGE_MODULE_H__

#include "Includes.h"

namespace NativePack
{
    class AsyncCallback
    {
      public:
        static void callAsyncHandler(uv_async_t* handle);
        AsyncCallback(
            Local<Function>     callBack,
            Local<Function>     completeCb,
            bool                isLoop           = false,
            callAsyncHandlerPtr callAsyncHandler = AsyncCallback::callAsyncHandler);
        static void          Init(Local<Function> callBack, Local<Function> completeCb, bool isLoop = false);
        uv_async_t*          handle;
        Persistent<Function> callback;
        Persistent<Function> completeCallback;
        bool                 isLoop = false;
    };
} // namespace NativePack

#endif
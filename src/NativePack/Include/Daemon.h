#ifndef __NATIVEPACK_MODULE_DAEMON_H__
#define __NATIVEPACK_MODULE_DAEMON_H__

#include "Includes.h"

namespace NativePack
{
    class Daemon : public node::ObjectWrap
    {
      public:
        static Local<Function>         Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
        static void                    New(FunctionCallbackArg args);
        static void                    Callback(uv_poll_t* handle, int status, int events);
        static void                    Start(FunctionCallbackArg args);
        static void                    onNewConnection(uv_stream_t* stream, int status);
        static void                    conCb(uv_connect_t* conn, int status);
        static void                    Connect(FunctionCallbackArg args);
        static void                    onPipeConnect(uv_connect_t* req, int status);
        Local<Object>*                 that;
        static Local<FunctionTemplate> createClass(Isolate* isolate);
        Daemon(Local<Object> target, Isolate* isolate);
        static Daemon* Unwrap(Local<Object> daemonWrap);
        static int     Main();
        // size_t self_size() const override { return sizeof(*this); }
        uv_pipe_t*         handle;
        uv_connect_t*      handleCon;
        static const char* pipeAddress()
        {
            return "\\\\?\\pipe\\some.name";
        };
    };
} // namespace NativePack

#endif
#ifndef __NATIVEPACK_MODULE_FILEWATCHER_H__
#define __NATIVEPACK_MODULE_FILEWATCHER_H__
#include "Includes.h"

namespace NativePack
{
    class FileWatcherJS
    {
      public:
        static void            Start(FunctionCallbackArg args);
        static void            Callback(uv_fs_poll_t* handle, int status, const uv_stat_t* prev, const uv_stat_t* curr);
        static void            New(FunctionCallbackArg args);
        static Local<Function> Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
    };

    class FileWatcher : public node::ObjectWrap
    {
      public:
        Local<Object>* that;
        FileWatcher(Local<Object> target, Isolate* isolate);
        static FileWatcher* Unwrap(Local<Object> daemonWrap);
        uv_fs_poll_t*       handle;
    };
} // namespace NativePack

#endif
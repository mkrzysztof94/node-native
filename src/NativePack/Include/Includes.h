#ifndef __NATIVEPACK_MODULE_INC_H__
#define __NATIVEPACK_MODULE_INC_H__

#include <assert.h>
#include <ctime>
#include <iostream>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fcntl.h>

#include "node.h"
#include "node_buffer.h"
#include "v8.h"
#include <env.h>
#include "node_wrap.h"
#include "node_platform.h"
#include "uv.h"
#include "v8-util.h"
// #include "v8-platform.h"
// #include "node_internals.h"
#include "node_object_wrap.h"
#include "util.h"
#include "async-wrap.h"
#include "module_wrap.h"
// #include "deps/uv/tree.h"

namespace NativePack
{
    using std::map;
    using v8::Context;
    using v8::Function;
    using v8::FunctionCallback;
    using v8::FunctionCallbackInfo;
    using v8::FunctionTemplate;
    using v8::Global;
    using v8::Handle;
    using v8::HandleScope;
    using v8::Integer;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::ObjectTemplate;
    using v8::Persistent;
    using v8::ReturnValue;
    using v8::String;
    using v8::Value;

    class Module;
    class Async;
    struct Work;
    class Async;
    class AsyncCallback;
    class Obj;
    class AsyncMessage;
    class Terminal;
    class TerminalJS;
    class TerminalNAdapter;
    class FileWatcher;
    class Daemon;
    class Logger;
    class LoggerJS;
    class LoggerNAdapter;
    class Utils;
    class Pipe;

    enum LoggerPrefixes
    {
        None,
        Info,
        Error,
        Warning,
        Debug
    };
} // namespace NativePack

#include "TypeDef.h"

#endif
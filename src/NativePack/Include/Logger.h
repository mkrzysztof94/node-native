#ifndef __NATIVEPACK_MODULE_LOGGER_H__
#define __NATIVEPACK_MODULE_LOGGER_H__

#include "Includes.h"

namespace NativePack
{
    class LoggerJS
    {
      public:
        static Local<Object>   Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
        static void            New(FunctionCallbackArg args);
        static LoggerNAdapter* Unwrap(Local<Object> self);
    };

    class Logger
    {
      public:
        static void        Log(const char* log, LoggerPrefixes type = LoggerPrefixes::None);
        static void        Log(std::string log, LoggerPrefixes type = LoggerPrefixes::None);
        static void        Log(Local<String> log, LoggerPrefixes type = LoggerPrefixes::None);
        static void        OnOpen(uv_fs_t* openReq);
        static void        Write(uv_fs_t* req, std::string msg, uv_fs_cb cb = Logger::OnWrite);
        static void        OnWrite(uv_fs_t* file);
        static void        OnClose(uv_fs_t* file);
        static std::string FormatNumber(int number);

        Logger(const char* As, const char* filePath);
        Logger(const char* As);

        void log(const char* log, LoggerPrefixes type = LoggerPrefixes::None);
        void log(std::string log, LoggerPrefixes type = LoggerPrefixes::None);
        void log(Local<String> log, LoggerPrefixes type = LoggerPrefixes::None);
        void error(const char* log);
        void error(std::string log);
        void error(Local<String> log);
        void info(const char* log);
        void info(std::string log);
        void info(Local<String> log);
        void warning(const char* log);
        void warning(std::string log);
        void warning(Local<String> log);
        void debug(const char* log);
        void debug(std::string log);
        void debug(Local<String> log);

        int errorPrinter(const char* log, int errorStatus);
        int errorPrinter(std::string log, int errorStatus);
        int errorPrinter(Local<String> log, int errorStatus);

        bool               logToFile = false;
        bool               logToOut  = true;
        static const char* prefixes[5];
        static const char* prefixesColor[5];

      protected:
        const char* author;
        uv_fs_t     openReq;
        uv_fs_t     writeReq;
        uv_fs_t     closeReq;
        uv_loop_t   loop;
    };

    class LoggerNAdapter : public node::ObjectWrap, public NativePack::Logger
    {
      public:
        LoggerNAdapter(Local<Object> target, Isolate* iso, const char* as = "NoName");
        LoggerNAdapter(Local<Object> target, Isolate* iso, const char* as, const char* fileName);
        static void JSError(FunctionCallbackArg args);
        static void JSInfo(FunctionCallbackArg args);
        static void JSWarning(FunctionCallbackArg args);
    };
} // namespace NativePack
#endif
#ifndef __NATIVEPACK_MODULE_MODULE_H__
#define __NATIVEPACK_MODULE_MODULE_H__

#include "Includes.h"

namespace NativePack
{
    class Module
    {
      public:
        static void               Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
        static Isolate*           moduleIsolate;
        static v8::Global<Object> globalScope;
        static node::Environment* nodeEnv;
        static void               Test(FunctionCallbackArg args);
    };
} // namespace NativePack
#endif
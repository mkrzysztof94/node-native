#ifndef __NATIVEPACK_MODULE_PIPE_H__
#define __NATIVEPACK_MODULE_PIPE_H__

#include "Includes.h"

namespace NativePack
{
    class Pipe
    {
      public:
        static Local<Object> Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
        static const char*   pipeAddress;
        static const char*   logAddress;

        class Server
        {
          public:
            Server();
            uv_pipe_t*  pipe;
            uv_loop_t*  loop;
            PipeMap     clientMap;
            std::string message;
        };

        class ServerJS : public Server, public node::ObjectWrap
        {
          public:
            ServerJS(Local<Object> target, Isolate* isolate);
            static Local<FunctionTemplate> getConstructor(Isolate* isolate);
            static void                    Open(FunctionCallbackArg args);
            static void                    Close(FunctionCallbackArg args);
            static void                    New(FunctionCallbackArg args);
            static ServerJS*               Unwrap(Local<Object> PipeServerWrap);
            static void                    onNewConnection(uv_stream_t* server, int status);
            static void                    Read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);

            Logger* logger;
        };

        class Client : public node::ObjectWrap
        {
          public:
            static Local<FunctionTemplate> getConstructor(Isolate* isolate);
            static void                    New(FunctionCallbackArg args);
            static void                    OnConnect(uv_connect_t* req, int status);
            static void                    Connect(FunctionCallbackArg args);
            static void                    Disconnect(FunctionCallbackArg args);
            static void                    Send(FunctionCallbackArg args);
            static void                    Read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);
            static Client*                 Unwrap(Local<Object> PipeClientWrap);
            Client(Local<Object> target, Isolate* isolate);
            uv_pipe_t*   pipe;
            uv_connect_t handleCon;
            Logger*      logger;
        };

        struct ServerConnection_s
        {
            int       cliId;
            ServerJS* serv;
        };
        typedef struct ServerConnection_s ServerConnection;
    };
} // namespace NativePack

#endif
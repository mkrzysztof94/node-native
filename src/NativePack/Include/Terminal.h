#ifndef __NATIVEPACK_MODULE_TERMINAL_H__
#define __NATIVEPACK_MODULE_TERMINAL_H__

#include "Includes.h"

namespace NativePack
{
    class Terminal
    {
      public:
        // STATIC FUNCTIONS
        static void OnInput(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);
        static void OnActionKey(uv_stream_t* stream, Terminal* self, char* key);
        static void OnActionKeys(uv_stream_t* stream, Terminal* self, char* keys, ssize_t size);

        // Object members
        Terminal(bool setLogger = true);
        Terminal(const char* logPath);
        void Write(const char* messageChar, LoggerPrefixes logType = LoggerPrefixes::None);
        void Write(std::string message, LoggerPrefixes logType = LoggerPrefixes::None);
        void Write(std::string str, TerminalWriteOptions* opt, LoggerPrefixes logType = LoggerPrefixes::None);
        void EvalInput(const char* evalStr, bool print_result = true, bool report_exceptions = true);
        void Complete(std::string evalStr, bool print_result = true, bool report_exceptions = true);

        void                  TtyInit();
        uv_tty_t              ttyOut;
        uv_tty_t              ttyIn;
        uv_write_t            writeReq;
        uv_buf_t              buf;
        uv_loop_t*            loop;
        TerminalWriteOptions* writeOpts;
        std::string           input;
        Logger*               logs;
        Local<Object>         jsThis;
        Isolate*              iso;
    };

    class TerminalJS
    {
      public:
        static Local<Function>   Initialize(Local<Object> exports, Local<Value> module, Local<Context> context);
        static void              New(FunctionCallbackArg args);
        static TerminalNAdapter* Unwrap(Local<Object> self);
    };

    class TerminalCompletition
    {
      public:
        static std::vector<std::string> GetCompletition(std::string target, Local<Context> cntx);
        static Local<v8::Array>         FindInArray(Local<v8::Array> arr, std::string target);
        static Local<Object>            FindCompletitionTarget(std::string target, Local<Context> cntx);
    };

    class TerminalExecutor
    {
      public:
        static void ReportOnException(Isolate* isolate, v8::TryCatch* try_catch);
        static bool ExecuteString(
            Isolate*      isolate,
            Local<String> source,
            Local<Value>  name,
            bool          print_result      = true,
            bool          report_exceptions = true);
    };

    class TerminalNAdapter : public NativePack::Terminal, public node::ObjectWrap
    {
      public:
        TerminalNAdapter(Local<Object> target, Isolate* iso);
        static void JSLog(FunctionCallbackArg args);
        static void JSWarn(FunctionCallbackArg args);
        static void JSError(FunctionCallbackArg args);
        static void JSWrite(FunctionCallbackArg args);
        static void JSGetWidth(FunctionCallbackArg args);
        static void JSGetHeight(FunctionCallbackArg args);
        static void JSReset(FunctionCallbackArg args);
        static void JSClearScreen(FunctionCallbackArg args);
    };
} // namespace NativePack

#endif

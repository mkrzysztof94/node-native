#ifndef __NATIVEPACK_MODULE_TYPEDEF_H__
#define __NATIVEPACK_MODULE_TYPEDEF_H__

namespace NativePack
{
    //============================== Func Ptr ==============================//
    typedef void (*onWorkAsyncComplete)(uv_work_t*, int);
    typedef void (*AsyncCallbackFunc)(uv_async_t*);
    typedef void (*WorkAsyncCompletePtr)(uv_work_t*, int);
    typedef void (*WorkAsyncPtr)(uv_work_t* handle);
    typedef void (*callAsyncHandlerPtr)(uv_async_t*);
    //============================== /Func Ptr ==============================//

    //============================== Map types ==============================//

    typedef std::map<std::string, FunctionCallback>  NameFunctionMap;
    typedef std::pair<std::string, FunctionCallback> NameFunctionPair;

    typedef std::map<std::string, Local<Object>>  NamePropertyMap;
    typedef std::pair<std::string, Local<Object>> NamePropertyPair;

    typedef std::map<const char*, Global<Object>*>  GlobalMap;
    typedef std::pair<const char*, Global<Object>*> GlobalPair;

    typedef std::map<std::string, Persistent<Object>>  GMap;
    typedef std::pair<std::string, Persistent<Object>> GPair;

    typedef std::map<int, uv_pipe_t*>  PipeMap;
    typedef std::pair<int, uv_pipe_t*> PipePair;
    //============================== /Map types ==============================//

    typedef const FunctionCallbackInfo<Value>& FunctionCallbackArg;

    struct write_req_s
    {
        uv_write_t req;
        uv_buf_t   buf;
    };
    typedef struct write_req_s write_req_t;

    struct TerminalWorker_s
    {
        uv_work_t request;
        Terminal* handle;
    };
    typedef struct TerminalWorker_s TerminalWorker;

    struct TerminalWriteOptions_s
    {
        int         row          = 0;
        int         column       = 0;
        bool        clear        = false;
        int         fColor       = 37;
        int         bColor       = 40;
        int         winHeight    = 0;
        int         winWidth     = 0;
        const char* promptPrefix = ">";
    };

    typedef struct TerminalWriteOptions_s TerminalWriteOptions;
} // namespace NativePack
#endif
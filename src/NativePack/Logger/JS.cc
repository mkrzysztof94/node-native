#include "Includes.h"
#include "Logger.h"
#include "Utils.h"

namespace NativePack
{
    using NativePack::Logger;
    using NativePack::LoggerNAdapter;

    Local<Object> LoggerJS::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        return Utils::JSClassBuilder::Create("Logger", LoggerJS::New)
            ->AddFunction("error", LoggerNAdapter::JSError)
            ->AddFunction("info", LoggerNAdapter::JSInfo)
            ->AddFunction("warning", LoggerNAdapter::JSWarning)
            ->BuildClass(exports->GetIsolate())
            ->GetFunction();
    }

    void LoggerJS::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that       = args.This();
        int           argsLength = (int) args.Length();

        if (argsLength == 2 && args[0]->IsString() && args[1]->IsString())
            new LoggerNAdapter(that, args.GetIsolate(), Utils::ToChar(args[0]), Utils::ToChar(args[1]));
        else if (argsLength == 1 && args[0]->IsString())
            new LoggerNAdapter(that, args.GetIsolate(), Utils::ToChar(args[0]));
        else
            return Utils::ThrowErrorException("Function need minimum 1 String argument", isolate);

        args.GetReturnValue().Set(that);
    }

    LoggerNAdapter* LoggerJS::Unwrap(Local<Object> self)
    {
        return node::ObjectWrap::Unwrap<LoggerNAdapter>(self);
    }

} // namespace NativePack
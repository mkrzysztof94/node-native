#include "Includes.h"
#include "Logger.h"
#include "Utils.h"

namespace NativePack
{
    const char* Logger::prefixes[5]      = {"", "INFO:", "ERROR:", "WARN:", "DEBUG:"};
    const char* Logger::prefixesColor[5] = {"", "\033[1;34m", "\033[1;31m", "\033[1;33m", "\037[1;m"};

    void Logger::Log(const char* log, LoggerPrefixes type)
    {
        std::cout << Logger::prefixes[type] << " ===> " << log << std::endl;
    }

    void Logger::Log(std::string log, LoggerPrefixes type)
    {
        Logger::Log(log.c_str(), type);
    }

    void Logger::Log(Local<String> log, LoggerPrefixes type)
    {
        Logger::Log(log->GetExternalOneByteStringResource()->data(), type);
    }

    void Logger::OnOpen(uv_fs_t* openReq)
    {
        if (openReq->result >= 0)
            Logger::Write(openReq, std::string("Start logging"));
        else
        {
            fprintf(stderr, "logFileOpenCb error \%s", uv_err_name(openReq->result));
            exit(0);
        }
        // uv_fs_req_cleanup(openReq);
    }

    void Logger::OnWrite(uv_fs_t* file)
    {
        // Logger* self = static_cast<Logger*>(file->data);
        Utils::ErrorPrinter("Logger::OnWrite error: ", file->result);
        // uv_fs_close(uv_default_loop(), &self->closeReq, self->openReq.result, Logger::OnClose);
        // uv_fs_req_cleanup(file);
    }

    void Logger::OnClose(uv_fs_t* file)
    {
        Utils::ErrorPrinter("Logger::OnClose error closing file: ", file->result);
        uv_fs_req_cleanup(file);
    }

    Logger::Logger(const char* As, const char* filePath)
    {
        this->author        = As;
        this->logToFile     = (bool) true;
        this->openReq.data  = this;
        this->writeReq.data = this;
        this->closeReq.data = this;
        uv_loop_init(&this->loop);
        uv_run(&this->loop, uv_run_mode::UV_RUN_NOWAIT);
        this->openReq.data = this;
        Utils::ErrorPrinter("File open error", uv_fs_open(&this->loop, &this->openReq, filePath, 2, 1, Logger::OnOpen));
    }

    Logger::Logger(const char* As)
    {
        this->author = As;
    }

    void Logger::Write(uv_fs_t* req, std::string msg, uv_fs_cb cb)
    {
        uv_file fd          = req->result;
        Logger* self        = static_cast<Logger*>(req->data);
        self->writeReq.data = Utils::GetBuffer(msg);
        Utils::ErrorPrinter(
            "logFileOpenCb File write error",
            uv_fs_write(&self->loop, &self->writeReq, fd, (uv_buf_t*) self->writeReq.data, 1, -1, cb));
    }

    void Logger::log(const char* log, LoggerPrefixes type)
    {
        time_t      t   = time(0); // get time now
        struct tm*  now = localtime(&t);
        std::string msg = std::string("\r\n[")
                              .append(Logger::prefixes[type])
                              .append(this->author)
                              .append("][")
                              .append(Logger::FormatNumber(now->tm_mday))
                              .append("-")
                              .append(Logger::FormatNumber(now->tm_mon + 1))
                              .append("-")
                              .append(std::to_string(now->tm_year + 1900))
                              .append("][")
                              .append(Logger::FormatNumber(now->tm_hour))
                              .append(":")
                              .append(Logger::FormatNumber(now->tm_min))
                              .append(":")
                              .append(Logger::FormatNumber(now->tm_sec))
                              .append("] ===> ")
                              .append(log);

        if (this->logToOut == (bool) true)
            std::cout << Logger::prefixesColor[type] << msg << "\033[0m";
        if (this->logToFile == (bool) true)
            Logger::Write(&this->openReq, msg, Logger::OnWrite);
    }

    std::string Logger::FormatNumber(int number)
    {
        if (number >= 10)
            return std::to_string(number);
        else
            return std::string("0").append(std::to_string(number));
    }

    void Logger::log(std::string log, LoggerPrefixes type)
    {
        this->log(log.c_str(), type);
    }

    void Logger::log(Local<String> log, LoggerPrefixes type)
    {
        this->log(log->GetExternalOneByteStringResource()->data(), type);
    }

    void Logger::error(const char* log)
    {
        this->log(log, LoggerPrefixes::Error);
    }

    void Logger::error(std::string log)
    {
        this->log(log.c_str(), LoggerPrefixes::Error);
    }

    void Logger::error(Local<String> log)
    {
        this->log(log, LoggerPrefixes::Error);
    }

    void Logger::info(const char* log)
    {
        this->log(log, LoggerPrefixes::Info);
    }

    void Logger::info(std::string log)
    {
        this->log(log.c_str(), LoggerPrefixes::Info);
    }

    void Logger::info(Local<String> log)
    {
        this->log(log, LoggerPrefixes::Info);
    }

    void Logger::warning(const char* log)
    {
        this->log(log, LoggerPrefixes::Warning);
    }

    void Logger::warning(std::string log)
    {
        this->log(log.c_str(), LoggerPrefixes::Warning);
    }

    void Logger::warning(Local<String> log)
    {
        this->log(log, LoggerPrefixes::Warning);
    }

    void Logger::debug(const char* log)
    {
        this->log(log, LoggerPrefixes::Debug);
    }

    void Logger::debug(std::string log)
    {
        this->log(log.c_str(), LoggerPrefixes::Debug);
    }

    void Logger::debug(Local<String> log)
    {
        this->log(log, LoggerPrefixes::Debug);
    }

    int Logger::errorPrinter(const char* log, int errorStatus)
    {
        if (errorStatus < 0)
            this->log(std::string(log).append(": ").append(uv_strerror(errorStatus)), LoggerPrefixes::Error);
        return errorStatus;
    }

    int Logger::errorPrinter(std::string log, int errorStatus)
    {
        if (errorStatus < 0)
            this->log(log.append(": ").append(uv_strerror(errorStatus)));
        return errorStatus;
    }

    int Logger::errorPrinter(Local<String> log, int errorStatus)
    {
        if (errorStatus < 0)
            this->log(log, LoggerPrefixes::Error);
        return errorStatus;
    }

} // namespace NativePack
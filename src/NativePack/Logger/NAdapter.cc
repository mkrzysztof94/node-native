#include "Includes.h"
#include "Logger.h"
#include "Utils.h"

namespace NativePack
{
    LoggerNAdapter::LoggerNAdapter(Local<Object> target, Isolate* iso, const char* as)
        : Logger(as)
    {
        HandleScope scope(iso);
        this->Wrap(target);
        this->Ref();
    }

    LoggerNAdapter::LoggerNAdapter(Local<Object> target, Isolate* iso, const char* as, const char* fileName)
        : Logger(as, fileName)
    {
        HandleScope scope(iso);
        this->Wrap(target);
        this->Ref();
    }

    void LoggerNAdapter::JSError(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        LoggerNAdapter* self = LoggerJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->error(Utils::ToChar(args[i]));

        std::cout << std::endl;

        args.GetReturnValue().SetUndefined();
    }

    void LoggerNAdapter::JSInfo(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        LoggerNAdapter* self = LoggerJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->info(Utils::ToChar(args[i]));

        std::cout << std::endl;

        args.GetReturnValue().SetUndefined();
    }

    void LoggerNAdapter::JSWarning(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        LoggerNAdapter* self = LoggerJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->warning(Utils::ToChar(args[i]));

        std::cout << std::endl;

        args.GetReturnValue().SetUndefined();
    }
} // namespace NativePack
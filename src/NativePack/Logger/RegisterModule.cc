#include "Includes.h"
#include "Utils.h"
#include "Logger.h"

namespace NativePack
{

    static void RegisterLoggerModule(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*      iso = context->Global()->GetIsolate();
        HandleScope   Scope(iso);
        Local<Object> logger    = NativePack::LoggerJS::Initialize(exports, module, context);
        Local<String> loggerKey = String::NewFromUtf8(iso, "Logger");
        context->Global()->Set(loggerKey, logger);
        exports->Set(loggerKey, logger);
        node::loader::ModuleWrap::Initialize(context->Global(), module, context);
    }

} // namespace NativePack

NODE_MODULE_CONTEXT_AWARE(NODE_GYP_MODULE_NAME, NativePack::RegisterLoggerModule);
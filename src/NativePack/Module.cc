#include "Includes.h"
#include "Utils.h"
#include "Pipe.h"
#include "Daemon.h"
#include "FileWatcher.h"
#include "Terminal.h"
#include "Module.h"

namespace NativePack
{
    Isolate*           Module::moduleIsolate;
    v8::Global<Object> Module::globalScope;
    node::Environment* Module::nodeEnv;
    using NativePack::TerminalJS;

    void Module::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Module::moduleIsolate = context->Global()->GetIsolate();
        HandleScope Scope(Module::moduleIsolate);
        Module::globalScope.Reset(Module::moduleIsolate, context->Global());
        Module::nodeEnv = node::Environment::GetCurrent(Module::moduleIsolate);
        Module::nodeEnv->AssignToContext(context);
        TerminalJS              terminalJS = *new TerminalJS();
        Utils::JSObjectBuilder* builder
            = Utils::JSObjectBuilder::Create()
                  ->AddProperty("Pipe", Pipe::Initialize(exports, module, context))
                  ->AddProperty("Daemon", Daemon::Initialize(exports, module, context))
                  ->AddProperty("FileWatcher", FileWatcherJS::Initialize(exports, module, context))
                  ->AddProperty("Terminal", terminalJS.Initialize(exports, module, context))
                  ->AddProperty("Global", context->Global());
        builder->BuildObject(Module::globalScope.Get(Module::moduleIsolate), Module::moduleIsolate, false);
        builder->BuildObject(exports, Module::moduleIsolate);
        node::loader::ModuleWrap::Initialize(Module::globalScope.Get(Module::moduleIsolate), module, context);
    }

    void Module::Test(FunctionCallbackArg args)
    {
        args.GetReturnValue().Set(String::NewFromUtf8(args.GetIsolate(), "TEST"));
    }
} // namespace NativePack

NODE_MODULE_CONTEXT_AWARE(NODE_GYP_MODULE_NAME, NativePack::Module::Initialize);
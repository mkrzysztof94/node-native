#include "Includes.h"
#include "Logger.h"
#include "Pipe.h"
#include "Utils.h"

namespace NativePack
{
    using Client = NativePack::Pipe::Client;

    Local<FunctionTemplate> Client::getConstructor(Isolate* isolate)
    {
        return Utils::JSClassBuilder::Create("PipeClient", Pipe::Client::New)
            ->AddFunction("Connect", Client::Connect)
            ->AddFunction("Disconnect", Client::Disconnect)
            ->AddFunction("Send", Client::Send)
            ->BuildClass(isolate);
    }

    void Client::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that = args.This();
        new Client(that, isolate);
        args.GetReturnValue().Set(that);
    }

    void Client::OnConnect(uv_connect_t* req, int status)
    {
        Client* self = static_cast<Client*>(req->data);
        if (status >= 0)
        {
            self->logger->info(std::string("CONNECTED with status: ").append(std::to_string(status)));
            self->logger->errorPrinter(
                "Client::onNewConnection read start error :",
                uv_read_start((uv_stream_t*) self->pipe, Utils::allocBuffer, Client::Read));
        }
        else
        {
            self->logger->errorPrinter("Client::OnConnect with status: ", status);
        }
    }

    void Client::Connect(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);
        Client*     pipeClient = Client::Unwrap(args.This());
        pipeClient->pipe       = (uv_pipe_t*) malloc(sizeof(uv_pipe_t));
        pipeClient->logger->errorPrinter(
            "Client::Connect pipe init error: ", uv_pipe_init(uv_default_loop(), pipeClient->pipe, 1));
        pipeClient->pipe->data     = pipeClient;
        pipeClient->handleCon.data = pipeClient;
        uv_pipe_connect(&pipeClient->handleCon, pipeClient->pipe, Pipe::pipeAddress, Client::OnConnect);
    }

    void Client::Disconnect(FunctionCallbackArg args)
    {
        Isolate*    iso = args.GetIsolate();
        HandleScope scope(iso);
        Client*     cli = Client::Unwrap(args.This());
        uv_close((uv_handle_t*) cli->pipe, NULL);
        uv_close((uv_handle_t*) &cli->handleCon, NULL);
    }

    Client::Client(Local<Object> target, Isolate* isolate)
    {
        HandleScope scope(isolate);
        this->Wrap(target);
        this->Ref();
        this->logger = new Logger("PipeClient", "/media/adi/Nowy/nativ-node/NativNode/tmp/DebugPipeClient.txt");
    }

    void Client::Send(FunctionCallbackArg args)
    {
        Isolate*    iso = args.GetIsolate();
        HandleScope scope(iso);
        if ((int) args.Length() == 0 || !args[0]->IsString())
            return Utils::ThrowErrorException("Function need string as first argument", iso);

        Client*     cli     = Client::Unwrap(args.This());
        std::string message = Utils::ToStr(args[0]);
        Utils::writeStringToStream((uv_stream_t*) cli->pipe, message);
        args.GetReturnValue().Set(
            String::NewFromUtf8(iso, std::string("Sended message: ").append(message.c_str()).c_str()));
    }

    void Client::Read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
    {
        Client* self = static_cast<Client*>(stream->data);
        self->logger->info(std::string("Reading, Size to read : ").append(std::to_string((int) nread).c_str()).c_str());

        if (0 >= (int) nread)
        {
            self->logger->error("End of read !");
            // uv_read_stop(stream);
        }
        else if ((int) nread > 0)
        {
            // char* msg = (char*) malloc((int) nread);
            // memcpy(msg, buf->base, (int) nread);
            self->logger->info(buf->base);
            // free(buf->base);
            // free(msg);
        }
    }

    Client* Client::Unwrap(Local<Object> PipeClientWrap)
    {
        return node::ObjectWrap::Unwrap<Client>(PipeClientWrap);
    }
} // namespace NativePack
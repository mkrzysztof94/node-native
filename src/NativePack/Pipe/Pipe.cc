#include "Includes.h"
#include "Logger.h"
#include "Utils.h"
#include "Pipe.h"

namespace NativePack
{
    const char* NativePack::Pipe::pipeAddress = "/media/adi/Nowy/nativ-node/NativNode/tmp/pipe.sock";
    const char* NativePack::Pipe::logAddress  = "/media/adi/Nowy/nativ-node/NativNode/tmp/DebugLog.txt";

    Local<Object> Pipe::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate* isolate = exports->GetIsolate();
        return Utils::JSObjectBuilder::Create()
            ->AddProperty("PipeServer", ServerJS::getConstructor(isolate)->GetFunction())
            ->AddProperty("PipeClient", Client::getConstructor(isolate)->GetFunction())
            ->BuildObject(isolate);
    }
}
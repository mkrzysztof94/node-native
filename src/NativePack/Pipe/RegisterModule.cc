#include "Includes.h"
#include "Utils.h"
#include "Pipe.h"

namespace NativePack
{

    static void RegisterPipeModule(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*      iso = context->Global()->GetIsolate();
        HandleScope   Scope(iso);
        Local<Object> pipe    = NativePack::Pipe::Initialize(exports, module, context);
        Local<String> pipeKey = String::NewFromUtf8(iso, "Pipe");
        context->Global()->Set(pipeKey, pipe);
        exports->Set(pipeKey, pipe);
        node::loader::ModuleWrap::Initialize(context->Global(), module, context);
    }

} // namespace NativePack

NODE_MODULE_CONTEXT_AWARE(NODE_GYP_MODULE_NAME, NativePack::RegisterPipeModule);
#include "Includes.h"
#include "Logger.h"
#include "Pipe.h"
#include "Utils.h"

namespace NativePack
{
    using NativePack::Logger;
    using node::AsyncWrap;
    using v8::Context;
    using v8::Function;
    using v8::FunctionTemplate;
    using v8::HandleScope;
    using v8::Integer;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::ObjectTemplate;
    using v8::String;
    using v8::Value;
    using Server   = NativePack::Pipe::Server;
    using ServerJS = NativePack::Pipe::ServerJS;
    using Client   = NativePack::Pipe::Client;

    ServerJS::ServerJS(Local<Object> target, Isolate* isolate)
        : Server()
    {
        HandleScope scope(isolate);
        this->Wrap(target);
        this->Ref();
        this->logger = new Logger("PipeServer", Pipe::logAddress);
    }

    Local<FunctionTemplate> ServerJS::getConstructor(Isolate* isolate)
    {
        return Utils::JSClassBuilder::Create("PipeServer", ServerJS::New)
            ->AddFunction("Open", ServerJS::Open)
            ->AddFunction("Close", ServerJS::Close)
            ->BuildClass(isolate);
    }

    void ServerJS::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that = args.This();
        new ServerJS(that, args.GetIsolate());
        args.GetReturnValue().Set(that);
    }

    void ServerJS::Open(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);
        ServerJS*   self = ServerJS::Unwrap(args.This());
        self->logger->info("Server start");
        self->pipe = new uv_pipe_t();
        self->loop = uv_default_loop();

        self->logger->errorPrinter(
            "[Server::Open] Pipe init error: ", uv_pipe_init(self->loop, self->pipe, 0)); /* ipc */

        self->logger->errorPrinter("[Server::Open] Pipe bind error: ", uv_pipe_bind(self->pipe, Pipe::pipeAddress));

        self->pipe->data = (void*) self;

        self->logger->errorPrinter(
            "[Server::Open] Pipe listen error: ", uv_listen((uv_stream_t*) self->pipe, 128, ServerJS::onNewConnection));

#if defined __WIN32__
        std::wstring wstr = std::to_wstring(*server->pipe->name);
        self->logger->info(std::string("Name: ") + std::string(wstr.begin(), wstr.end(), wstr.get_allocator()));
        self->logger->info(std::string("Activecnt: ") + std::to_string(self->pipe.activecnt));
        self->logger->info(
            std::string("uv_pipe_pending_count: ") + std::to_string(uv_pipe_pending_count(server->pipe)));
#endif
    }

    void ServerJS::Close(FunctionCallbackArg args)
    {
        Isolate*    iso = args.GetIsolate();
        HandleScope scope(iso);
        ServerJS*   server = ServerJS::Unwrap(args.This());
        uv_close((uv_handle_t*) &server->pipe, NULL);
    }

    ServerJS* ServerJS::Unwrap(Local<Object> PipeServerWrap)
    {
        return node::ObjectWrap::Unwrap<ServerJS>(PipeServerWrap);
    }

    void ServerJS::onNewConnection(uv_stream_t* server, int status)
    {
        ServerJS* self      = static_cast<ServerJS*>(server->data);
        int       id        = (int) self->clientMap.size();
        self->clientMap[id] = (uv_pipe_t*) malloc(sizeof(uv_pipe_t));
        self->logger->info(std::string("New connection with status: ").append(std::to_string(status)));
        self->logger->errorPrinter(
            "Server::onNewConnection uv_pipe_init error: ", uv_pipe_init(self->pipe->loop, self->clientMap[id], 0));

        if (self->logger->errorPrinter(
                "New connection accept error: ", uv_accept(server, (uv_stream_t*) self->clientMap[id]))
            == 0)
        {
            self->logger->info("Pipe for new connection accepted");
            self->clientMap[id]->data = (void*) new ServerConnection{id, self};
            self->logger->errorPrinter(
                "Server::onNewConnection read start error :",
                uv_read_start((uv_stream_t*) self->clientMap[id], Utils::allocBuffer, ServerJS::Read));
            Utils::writeStringToStream((uv_stream_t*) self->clientMap[id], std::string("Połączono do servera"));
        }
        else
        {
            uv_close((uv_handle_t*) self->clientMap[id], NULL);
        }
    }

    void ServerJS::Read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
    {
        ServerConnection* con = static_cast<ServerConnection*>(stream->data);
        con->serv->logger->info(
            std::string("Reading, Size to read :").append(std::to_string((int) nread).c_str()).c_str());

        if (con->serv->logger->errorPrinter("Connection lost closing ==> ", nread) < 0)
        {
            // uv_read_stop(stream);
            // uv_close((uv_handle_t*) con->serv->clientMap[con->cliId], NULL);
            con->serv->logger->info("Connection closed");
        }
        else if ((int) nread > (int) 0)
        {
            Utils::writeStringToStream(
                (uv_stream_t*) con->serv->clientMap[con->cliId],
                std::string("Odebrano: ").append(std::to_string((int) nread).c_str()).c_str());
            con->serv->logger->info(buf->base);
            // free(buf->base);
            // std::cout << msg << std::endl;
        }
    }

} // namespace NativePack
#include "Includes.h"
#include "Logger.h"
#include "Pipe.h"
#include "Utils.h"

namespace NativePack
{
    using NativePack::Logger;
    using node::AsyncWrap;
    using v8::Context;
    using v8::Function;
    using v8::FunctionTemplate;
    using v8::HandleScope;
    using v8::Integer;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::ObjectTemplate;
    using v8::String;
    using v8::Value;
    using Server = NativePack::Pipe::Server;
    using Client = NativePack::Pipe::Client;

    Server::Server()
    {
        this->message   = *new std::string("");
        this->clientMap = *new PipeMap();
    }

} // namespace NativePack
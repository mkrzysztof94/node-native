#include "Includes.h"
#include "Module.h"
#include "Logger.h"
#include "Utils.h"
#include "Terminal.h"

namespace NativePack
{

    std::vector<std::string> TerminalCompletition::GetCompletition(std::string target, Local<Context> cntx)
    {
        Local<Object> targetObj = target.find(".") <= 0 ? cntx->Global() : FindCompletitionTarget(target, cntx);
        std::vector<std::string> out;
        std::string              searchKey = std::string();
        int                      i         = target.length() - 1;

        for (i = (int) 0; i >= (int) 0; i--)
            if (target.compare(i, 1, ".") != 0)
                searchKey = target.substr(i).c_str();
            else
                break;

        Local<v8::Array> completeArr = FindInArray(
            targetObj->GetOwnPropertyNames(cntx, v8::PropertyFilter::SKIP_SYMBOLS).ToLocalChecked(), searchKey);

        for (i = 0; i < (int) completeArr->Length(); i++)
            out.push_back(Utils::ToStr(completeArr->Get(i)).c_str());

        return out;
    }

    Local<v8::Array> TerminalCompletition::FindInArray(Local<v8::Array> arr, std::string target)
    {
        Local<v8::Array> result = v8::Array::New(arr->GetIsolate());

        for (int i = 0; i < (int) arr->Length(); i++)
            if (!Utils::ToStr(arr->Get(i)).compare(0, target.length(), target.c_str()))
                result->Set(result->Length(), arr->Get(i));

        return result;
    }

    Local<Object> TerminalCompletition::FindCompletitionTarget(std::string target, Local<Context> cntx)
    {
        Isolate*      iso       = cntx->GetIsolate();
        Local<Object> targetObj = cntx->Global();
        std::string   propKey   = std::string();
        Local<String> tmpKey;

        for (int i = 0; i < (int) target.length(); i++)
            if (!target.compare(i, 1, "."))
            {
                tmpKey = String::NewFromUtf8(iso, propKey.c_str());
                propKey.clear();

                if (targetObj->Has(tmpKey))
                    targetObj = targetObj->Get(tmpKey)->ToObject(iso);
                else
                    break;
            }
            else
                propKey += target[i];

        return targetObj;
    }

} // namespace NativePack
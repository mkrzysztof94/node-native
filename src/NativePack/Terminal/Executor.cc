#include "Includes.h"
#include "Module.h"
#include "Logger.h"
#include "Utils.h"
#include "Terminal.h"

namespace NativePack
{

    void TerminalExecutor::ReportOnException(v8::Isolate* isolate, v8::TryCatch* try_catch)
    {
        HandleScope        handle_scope(isolate);
        String::Utf8Value  exception(try_catch->Exception());
        const char*        exception_string = Utils::ToCString(exception);
        Local<v8::Message> message          = try_catch->Message();

        // V8 didn't provide any extra information about this error; just print the exception.
        if (message.IsEmpty())
            return (void) fprintf(stderr, "%s\n", exception_string);

        Local<Context> context(isolate->GetCurrentContext());
        // Print line of source code.
        fprintf(
            stderr,
            "%s:%i: %s\n",
            Utils::Utils::ToChar(message->GetScriptOrigin().ResourceName()),
            message->GetLineNumber(context).FromJust(),
            exception_string);
        fprintf(stderr, "%s\n", Utils::Utils::ToChar(message->GetSourceLine(context).ToLocalChecked()));

        int start = message->GetStartColumn(context).FromJust();

        for (int i = 0; i < start; i++)
            fprintf(stderr, " ");
        for (int i = start; i < (int) message->GetEndColumn(context).FromJust(); i++)
            fprintf(stderr, "^");

        fprintf(stderr, "\n");

        v8::Local<v8::Value> stack_trace_string;

        if (try_catch->StackTrace(context).ToLocal(&stack_trace_string))
            if (stack_trace_string->IsString())
                if (stack_trace_string->ToString()->Length() > 0)
                    fprintf(stderr, "%s\n", Utils::Utils::ToChar(stack_trace_string));
    }

    bool TerminalExecutor::ExecuteString(
        Isolate* isolate, Local<String> source, Local<Value> name, bool print_result, bool report_exceptions)
    {
        Local<Object>    glo = isolate->GetCurrentContext()->Global();
        v8::HandleScope  handle_scope(glo->GetIsolate());
        v8::TryCatch     try_catch(glo->GetIsolate());
        Local<Context>   context = glo->CreationContext();
        v8::ScriptOrigin origin(name);

        Local<v8::Script> script;
        if (v8::Script::Compile(context, source, &origin).ToLocal(&script))
        {
            Local<Value> result;
            if (script->Run(context).ToLocal(&result))
            {
                assert(!try_catch.HasCaught());
                // If all went well and the result wasn't undefined then print the returned value.
                if (print_result && !result->IsUndefined())
                    printf("%s\n", Utils::ToChar(result));

                return true;
            }
            else
            {
                assert(try_catch.HasCaught());
                // Print errors that happened during execution.
                if (report_exceptions)
                    ReportOnException(isolate, &try_catch);

                return false;
            }
        }
        else
        {
            if (report_exceptions) // Print errors that happened during compilation.
                ReportOnException(isolate, &try_catch);
            return false;
        }
    }

} // namespace NativePack
#include "Includes.h"
#include "Logger.h"
#include "Utils.h"
#include "Terminal.h"

namespace NativePack
{
    using NativePack::TerminalNAdapter;

    Local<Function> TerminalJS::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        return Utils::JSClassBuilder::Create("Terminal", TerminalJS::New)
            ->AddFunction("Log", TerminalNAdapter::JSLog)
            ->AddFunction("Warn", TerminalNAdapter::JSWarn)
            ->AddFunction("Error", TerminalNAdapter::JSError)
            ->AddFunction("Write", TerminalNAdapter::JSWrite)
            ->AddFunction("GetWidth", TerminalNAdapter::JSGetWidth)
            ->AddFunction("GetHeight", TerminalNAdapter::JSGetHeight)
            ->AddFunction("Reset", TerminalNAdapter::JSReset)
            ->AddFunction("ClearScreen", TerminalNAdapter::JSClearScreen)
            ->BuildClass(exports->GetIsolate())
            ->GetFunction();
    }

    void TerminalJS::New(FunctionCallbackArg args)
    {
        Isolate*    isolate = args.GetIsolate();
        HandleScope scope(isolate);

        if (!args.IsConstructCall())
            return Utils::ThrowErrorException("Function have to be called with new", isolate);

        Local<Object> that = args.This();
        new TerminalNAdapter(that, args.GetIsolate());
        args.GetReturnValue().Set(that);
    }

    TerminalNAdapter* TerminalJS::Unwrap(Local<Object> self)
    {
        return node::ObjectWrap::Unwrap<TerminalNAdapter>(self);
    }
} // namespace NativePack
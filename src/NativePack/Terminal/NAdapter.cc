#include "Includes.h"
#include "Logger.h"
#include "Module.h"
#include "Utils.h"
#include "Terminal.h"

namespace NativePack
{
    using NativePack::LoggerPrefixes;
    using NativePack::TerminalJS;

    TerminalNAdapter::TerminalNAdapter(Local<Object> target, Isolate* iso)
        : Terminal("/media/adi/Nowy/nativ-node/NativNode/tmp/TerminalLog.txt")
    {
        HandleScope scope(iso);
        this->Wrap(target);
        this->Ref();
        this->jsThis = target;
        this->iso    = target->GetIsolate();
        this->TtyInit();
        this->Write(this->input, this->writeOpts);
    }
    void TerminalNAdapter::JSWrite(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        TerminalNAdapter* self = TerminalJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->Write(Utils::ToChar(args[i]), LoggerPrefixes::Info);
    }

    void TerminalNAdapter::JSClearScreen(FunctionCallbackArg args)
    {
        TerminalNAdapter* self = TerminalJS::Unwrap(args.This());
        Utils::writeStringToStream((uv_stream_t*) &self->ttyOut, std::string("\033[2J"));
    }

    void TerminalNAdapter::JSReset(FunctionCallbackArg args)
    {
        uv_tty_reset_mode();
    }

    void TerminalNAdapter::JSGetWidth(FunctionCallbackArg args)
    {
        args.GetReturnValue().Set(String::NewFromUtf8(
            args.GetIsolate(), std::to_string(TerminalJS::Unwrap(args.This())->writeOpts->winWidth).c_str()));
    }

    void TerminalNAdapter::JSGetHeight(FunctionCallbackArg args)
    {
        args.GetReturnValue().Set(String::NewFromUtf8(
            args.GetIsolate(), std::to_string(TerminalJS::Unwrap(args.This())->writeOpts->winHeight).c_str()));
    }

    void TerminalNAdapter::JSLog(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        TerminalNAdapter* self = TerminalJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->Write(Utils::ToChar(args[i]), LoggerPrefixes::None);

        args.GetReturnValue().SetUndefined();
    }

    void TerminalNAdapter::JSWarn(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        TerminalNAdapter* self = TerminalJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->Write(Utils::ToChar(args[i]), LoggerPrefixes::Warning);

        args.GetReturnValue().SetUndefined();
    }

    void TerminalNAdapter::JSError(FunctionCallbackArg args)
    {
        if ((int) args.Length() == 0)
            return Utils::ThrowErrorException("Function need minimum 1 String argument", args.GetIsolate());

        TerminalNAdapter* self = TerminalJS::Unwrap(args.This());

        for (int i = 0; i < (int) args.Length(); i++)
            if (args[i]->IsString())
                self->Write(Utils::ToChar(args[i]), LoggerPrefixes::Error);

        args.GetReturnValue().SetUndefined();
    }
} // namespace NativePack
#include "Includes.h"
#include "Terminal.h"
#include "Utils.h"

namespace NativePack
{

    static void RegisterTerminalModule(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*                iso = context->Global()->GetIsolate();
        HandleScope             Scope(iso);
        Utils::JSObjectBuilder* builder = Utils::JSObjectBuilder::Create()->AddProperty(
            "Terminal", TerminalJS::Initialize(exports, module, context));
        builder->BuildObject(context->Global(), iso, false);
        builder->BuildObject(exports, iso);
        node::loader::ModuleWrap::Initialize(context->Global(), module, context);
    }

} // namespace NativePack

NODE_MODULE_CONTEXT_AWARE(NODE_GYP_MODULE_NAME, NativePack::RegisterTerminalModule);
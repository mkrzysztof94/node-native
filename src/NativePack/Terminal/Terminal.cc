#include "Includes.h"
#include "Logger.h"
#include "Module.h"
#include "Utils.h"
#include "Terminal.h"

namespace NativePack
{
    using TCompletition = NativePack::TerminalCompletition;

    Terminal::Terminal(bool setLogger)
    {
        if (setLogger)
            this->logs = new Logger((char*) "Terminal");

        this->input     = *new std::string("");
        this->writeOpts = new TerminalWriteOptions();
        this->loop      = uv_default_loop();
    }

    Terminal::Terminal(const char* logPath)
        : Terminal(false)
    {
        this->logs = new Logger((char*) "Terminal", logPath);
    }

    void Terminal::TtyInit()
    {
        Utils::ErrorPrinter("Tty reset error: ", uv_tty_reset_mode());
        Utils::ErrorPrinter("Tty stdin init error: ", uv_tty_init(this->loop, &this->ttyIn, 0, 1));
        Utils::ErrorPrinter("Tty stdout out error: ", uv_tty_init(this->loop, &this->ttyOut, 1, 0));
        Utils::ErrorPrinter(
            "Tty stdout set mode error: ", uv_tty_set_mode(&this->ttyOut, uv_tty_mode_t::UV_TTY_MODE_RAW));
        Utils::ErrorPrinter(
            "Tty stdin set mode error: ", uv_tty_set_mode(&this->ttyIn, uv_tty_mode_t::UV_TTY_MODE_RAW));
        Utils::ErrorPrinter(
            "Tty get winsize error: ",
            uv_tty_get_winsize(&this->ttyOut, &this->writeOpts->winWidth, &this->writeOpts->winHeight));
        this->writeOpts->row = this->writeOpts->winHeight;
        this->ttyOut.data    = this;
        this->ttyIn.data     = this;
        if (this->ttyIn.type == 14)
            this->logs->errorPrinter(
                "UV read error: ", uv_read_start((uv_stream_t*) &this->ttyIn, Utils::allocBuffer, Terminal::OnInput));
        else
            this->logs->error("Tty type error");
    }

    void Terminal::Write(const char* messageChar, LoggerPrefixes logType)
    {
        Utils::writeStringToStream(
            (uv_stream_t*) &this->ttyOut,
            std::string(Logger::prefixesColor[logType]).append(messageChar).append("\033[0m"));
    }

    void Terminal::Write(std::string message, LoggerPrefixes logType)
    {
        this->Write(message.c_str(), logType);
    }

    void Terminal::Write(std::string str, TerminalWriteOptions* opt, LoggerPrefixes logType)
    {
        std::string msg = std::string("\033[H") + "\033[" + std::to_string(opt->row) + "B\033[0m"; // + "\033[00C";
        if (opt->clear)
            msg += "\033[3J";
        this->Write(msg.append(opt->promptPrefix).append(str), logType);
    }

    void Terminal::OnInput(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
    {
        Terminal* term = static_cast<Terminal*>(stream->data);

        if (nread == 1 && buf->base[0] > 31 && buf->base[0] < 127)
        {
            term->input += buf->base[0];
            term->Write(term->input.substr(term->input.length() - 1));
        }
        else if (nread == 1)
            Terminal::OnActionKey(stream, term, &buf->base[0]);
        else
            Terminal::OnActionKeys(stream, term, buf->base, nread);
    }

    void Terminal::OnActionKey(uv_stream_t* stream, Terminal* self, char* key)
    {

        switch (*key)
        {
            case 3: // ctrl+c
                uv_read_stop(stream);
                uv_close((uv_handle_t*) stream, NULL);
                exit(1);
                break;
            case 9: // tab
                self->Complete(self->input);
                break;
            case 13:
                self->input += "\n";
                self->Write(self->input.substr(self->input.length() - 1));
                break;
            case 18: // ctr+r
                self->EvalInput(self->input.c_str());
                break;
            case 127:
                if ((int) (self->input.length() - 1) >= (int) 0)
                {
                    self->Write(std::string("\b \b"));
                    self->input = self->input.substr(0, self->input.length() - 1);
                    self->Write(self->input, self->writeOpts);
                }

                break;
            default:
                std::cout << "\033[2B" << std::endl << "CHAR: " << std::to_string(*key) << std::endl;
        };
    }

    void Terminal::Complete(std::string evalStr, bool print_result, bool report_exceptions)
    {
        HandleScope scope(this->iso);
        int         col  = 0;
        int         line = 1;

        std::vector<std::string> compArr = TCompletition::GetCompletition(evalStr, this->iso->GetCurrentContext());

        this->Write("\033[2J\033[3J", LoggerPrefixes::None);

        for (int i = 0; i < (int) compArr.size(); i++)
        {
            this->Write(
                "\033[" + std::to_string(line) + ";" + std::to_string((int) ((col * 30) + 1)) + "H"
                    + compArr[i].c_str(),
                LoggerPrefixes::Info);

            if (col == 2)
            {
                col = 0;
                line++;
            }
            else
                col++;
        }

        this->Write("\n");
        this->Write(this->input, this->writeOpts);
    }

    void Terminal::EvalInput(const char* evalStr, bool print_result, bool report_exceptions)
    {
        Isolate*      iso = Isolate::GetCurrent();
        HandleScope   scope(iso);
        Local<String> src   = String::NewFromUtf8(iso, evalStr);
        Local<Value>  fname = String::NewFromUtf8(iso, "undefined").As<Value>();
        this->Write(
            std::string("\n\n[Eval]\n\n").append(this->input.c_str()).append("\n\n[/Eval]\n\n"), LoggerPrefixes::Info);
        this->input.clear();
        TerminalExecutor::ExecuteString(Module::moduleIsolate, src, fname, print_result, report_exceptions);
        this->Write("", this->writeOpts);
    }

    void Terminal::OnActionKeys(uv_stream_t* stream, Terminal* self, char* keys, ssize_t size)
    {
        std::string actionCode = std::to_string(keys[0]);

        for (int i = 1; i < size; i++)
            actionCode.append("::").append(std::to_string(keys[i]));

        std::cout << std::endl << "Action code: " << actionCode << std::endl;
    }

} // namespace NativePack
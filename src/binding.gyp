{
    "targets": [
        {
            "target_name": "Logger",
            "includes":[
                "./common.gypi"
            ],
            'include_path': [
                "NativePack/Include/"
            ],
            'link_settings': {
            },
            'dependencies': [

            ],
            "sources": [
                "NativePack/Utils/Utils.cc",
                "NativePack/Utils/JSClassBuilder.cc",
                "NativePack/Utils/JSObjectBuilder.cc",
                "NativePack/Logger/JS.cc",
                "NativePack/Logger/NAdapter.cc",
                "NativePack/Logger/Logger.cc",
                "NativePack/Logger/RegisterModule.cc"
            ]
        },
        {
            "target_name": "Pipe",
            "includes":[
                "./common.gypi"
            ],
            'include_path': [
                "NativePack/Include/"
            ],
            'link_settings': {
            },
            'dependencies': [

            ],
            "sources": [
                "NativePack/Logger/Logger.cc",
                "NativePack/Utils/Utils.cc",
                "NativePack/Utils/JSClassBuilder.cc",
                "NativePack/Utils/JSObjectBuilder.cc",
                "NativePack/Pipe/Pipe.cc",
                "NativePack/Pipe/Server/Server.cc",
                "NativePack/Pipe/Server/JS.cc",
                "NativePack/Pipe/Client.cc",
                "NativePack/Pipe/RegisterModule.cc"
            ]
        },
        {
            "target_name": "Terminal",
            "includes":[
                "./common.gypi"
            ],
            'include_path': [
                "NativePack/Include/"
            ],
            'link_settings': {
            },
            'dependencies': [

            ],
            "sources": [
                "NativePack/Logger/Logger.cc",
                "NativePack/Utils/Utils.cc",
                "NativePack/Utils/JSClassBuilder.cc",
                "NativePack/Utils/JSObjectBuilder.cc",
                "NativePack/Terminal/Terminal.cc",
                "NativePack/Terminal/NAdapter.cc",
                "NativePack/Terminal/JS.cc",
                "NativePack/Terminal/Completition.cc",
                "NativePack/Terminal/Executor.cc",
                "NativePack/Terminal/RegisterModule.cc"
            ]
        },
        {
            "target_name": "NativePack",
            "includes":[
                "./common.gypi"
            ],
            'include_path': [
                "NativePack/Include/"
            ],
            'link_settings': {
            },
            'dependencies': [

            ],
            "sources": [
                "NativePack/Module.cc",
                "NativePack/Async/Async.cc",
                "NativePack/Async/AsyncCallback.cc",
                "NativePack/Terminal/Terminal.cc",
                "NativePack/Terminal/JS.cc",
                "NativePack/Terminal/NAdapter.cc",
                "NativePack/Terminal/Completition.cc",
                "NativePack/Terminal/Executor.cc",
                "NativePack/Daemon/Daemon.cc",
                "NativePack/FileWatcher/FileWatcher.cc",
                "NativePack/FileWatcher/JS.cc",
                "NativePack/Pipe/Pipe.cc",
                "NativePack/Pipe/Server/Server.cc",
                "NativePack/Pipe/Server/JS.cc",
                "NativePack/Pipe/Client.cc",
                "NativePack/Logger/Logger.cc",
                "NativePack/Utils/Utils.cc",
                "NativePack/Utils/JSClassBuilder.cc",
                "NativePack/Utils/JSObjectBuilder.cc"
            ]
        }
    ]
}

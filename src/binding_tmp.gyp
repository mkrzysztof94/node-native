{
    "targets": [
        {
            "target_name": "Globals",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Globals.cc"
            ]
        },
        {
            "target_name": "Async",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Async/Async.cc",
                "./NativePack/Async/AsyncCallback.cc"
            ]
        },
        {
            "target_name": "Terminal",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Terminal/Terminal.cc",
                "./NativePack/Terminal/TerminalJS.cc"
            ]
        },
        {
            "target_name": "Daemon",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Daemon/Daemon.cc"
            ]
        },
        {
            "target_name": "FileWatcher",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/FileWatcher/FileWatcher.cc"
            ]
        },
        {
            "target_name": "Pipe",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Pipe/Pipe.cc",
                "./NativePack/Pipe/Server.cc",
                "./NativePack/Pipe/Client.cc"
            ]
        },
        {
            "target_name": "Logger",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Logger/Logger.cc"
            ]
        },
        {
            "target_name": "Utils",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Utils/Utils.cc",
                "./NativePack/Utils/JSClassBuilder.cc",
                "./NativePack/Utils/JSObjectBuilder.cc"
            ]
        },
        {
            "target_name": "NativePack",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Module.cc"
            ]
        }
        {
            "target_name": "NativePack",
            "includes":[
                "./common.gypi"
            ],
            "sources": [
                "./NativePack/Globals.cc",
                "./NativePack/Module.cc",
                "./NativePack/Async/Async.cc",
                "./NativePack/Async/AsyncCallback.cc",
                "./NativePack/Terminal/Terminal.cc",
                "./NativePack/Terminal/TerminalJS.cc",
                "./NativePack/Daemon/Daemon.cc",
                "./NativePack/FileWatcher/FileWatcher.cc",
                "./NativePack/Pipe/Pipe.cc",
                "./NativePack/Pipe/Server.cc",
                "./NativePack/Pipe/Client.cc",
                "./NativePack/Logger/Logger.cc",
                "./NativePack/Utils/Utils.cc",
                "./NativePack/Utils/JSClassBuilder.cc",
                "./NativePack/Utils/JSObjectBuilder.cc"
            ]
        },
    ]
}

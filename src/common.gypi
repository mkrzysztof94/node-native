{
    'variables': {
        'asan%': 0,
        'werror': '',                     # Turn off -Werror in V8 build.
        'visibility%': 'hidden',          # V8's visibility setting
        'target_arch%': 'ia32',           # set v8's target architecture
        'host_arch%': 'ia32',             # set v8's host architecture
        'want_separate_host_toolset%': 0, # V8 should not build target and host
        'library%': 'static_library',     # allow override to 'shared_library' for DLL/.so builds
        'component%': 'static_library',   # NB. these names match with what V8 expects
        'msvs_multi_core_compile': '0',   # we do enable multicore compiles, but not using the V8 way
        'python%': 'python',

        'node_shared%': 'false',
        'force_dynamic_crt%': 0,
        'node_use_v8_platform%': 'true',
        'node_use_bundled_v8%': 'true',
        'node_module_version%': '',

        'node_tag%': '',
        'uv_library%': 'static_library',

        'openssl_fips%': '',

        # Default to -O0 for debug builds.
        'v8_optimized_debug%': 0,

        # Enable disassembler for `--print-code` v8 options
        'v8_enable_disassembler': 1,

        # Don't bake anything extra into the snapshot.
        'v8_use_external_startup_data%': 0,

        # Some STL containers (e.g. std::vector) do not preserve ABI compatibility
        # between debug and non-debug mode.
        'disable_glibcxx_debug': 1,

        # Don't use ICU data file (icudtl.dat) from V8, we use our own.
        'icu_use_data_file_flag%': 0,

        'conditions': [
        ['GENERATOR=="ninja"', {
            'OBJ_DIR': '<(PRODUCT_DIR)/obj',
            'V8_BASE': '<(PRODUCT_DIR)/obj/deps/v8/src/libv8_base.a',
        }, {
            'OBJ_DIR%': '<(PRODUCT_DIR)/obj.target',
            'V8_BASE%': '<(PRODUCT_DIR)/obj.target/deps/v8/src/libv8_base.a',
        }],
        ['OS == "win"', {
            'os_posix': 0,
            'v8_postmortem_support%': 'false',
            'OBJ_DIR': '<(PRODUCT_DIR)/obj',
            'V8_BASE': '<(PRODUCT_DIR)/lib/v8_libbase.lib',
        }, {
            'os_posix': 1,
            'v8_postmortem_support%': 'true',
        }],
        ['OS== "mac"', {
            'OBJ_DIR%': '<(PRODUCT_DIR)/obj.target',
            'V8_BASE': '<(PRODUCT_DIR)/libv8_base.a',
        }],
        ['openssl_fips != ""', {
            'OPENSSL_PRODUCT': 'libcrypto.a',
        }, {
            'OPENSSL_PRODUCT': 'libopenssl.a',
        }],
        ['OS=="mac"', {
            'clang%': 1,
        }, {
            'clang%': 0,
        }],
        ],
    },
    'target_defaults': {
        'type': 'loadable_module',
        'win_delay_load_hook': 'true',
        'product_prefix': '',
        'conditions': [
            ['node_engine=="chakracore"', {
                'variables': {
                    'node_engine_include_dir%': 'deps/chakrashim/include'
                },
            }]
        ],
        'include_dirs': [
            '<(node_root_dir)/include/node',
            '<(node_root_dir)/src',
            './include/node/v8.9.1',
            './include/node/v8.9.1/src',
            './include/node/v8.9.1/deps/cares/include',
            './include/node/v8.9.1/deps/uv/include',
            './include/node/v8.9.1/deps/v8/include',
            './NativePack/Include'
            # './include/node/v8.9.0'
        ],
        'defines!': [
            'BUILDING_UV_SHARED=1',  # Inherited from common.gypi.
            'BUILDING_V8_SHARED=1',  # Inherited from common.gypi.
        ],
        'defines': [
            'NODE_GYP_MODULE_NAME=>(_target_name)',
            'MODULE_WRAP_NAME="module"',
            'USING_UV_SHARED=1',
            'USING_V8_SHARED=1',
            # Warn when using deprecated V8 APIs.
            'V8_DEPRECATION_WARNINGS=1',
            "NODE_WANT_INTERNALS=1",
            "BUILDING_NODE_EXTENSION=1",
            "V8_DEPRECATION_WARNINGS=1",
            "NODE_SHARED_MODE=1",
            "NODE_HAVE_SMALL_ICU=1"
        ],

        'target_conditions': [
            ['_type=="loadable_module"', {
                'product_extension': 'node',
                'defines': [
                    'BUILDING_NODE_EXTENSION'
                ],
                'xcode_settings': {
                    'OTHER_LDFLAGS': [
                        '-undefined dynamic_lookup'
                    ],
                },
            }],

            ['_type=="static_library"', {
                # set to `1` to *disable* the -T thin archive 'ld' flag.
                # older linkers don't support this flag.
                'standalone_static_library': '<(standalone_static_library)'
            }],

            ['_win_delay_load_hook=="true"', {
                # If the addon specifies `'win_delay_load_hook': 'true'` in its
                # binding.gyp, link a delay-load hook into the DLL. This hook ensures
                # that the addon will work regardless of whether the node/iojs binary
                # is named node.exe, iojs.exe, or something else.
                'conditions': [
                    ['OS=="win"', {
                        'sources': [
                            '<(node_gyp_dir)/src/win_delay_load_hook.cc',
                        ],
                        'msvs_settings': {
                            'VCLinkerTool': {
                                'DelayLoadDLLs': ['iojs.exe', 'node.exe'],
                                # Don't print a linker warning when no imports from either .exe
                                # are used.
                                'AdditionalOptions': ['/ignore:4199'],
                            },
                        },
                    }],
                ],
            }],
        ],

        'conditions': [
            ['OS=="mac"', {
                'defines': [
                    '_DARWIN_USE_64_BIT_INODE=1'
                ],
                'xcode_settings': {
                    'DYLIB_INSTALL_NAME_BASE': '@rpath'
                },
            }],
            ['OS=="aix"', {
                'ldflags': [
                    '-Wl,-bimport:<(node_exp_file)'
                ],
            }],
            ['OS=="win"', {
                'conditions': [
                    ['node_engine=="chakracore"', {
                        'library_dirs': ['<(node_root_dir)/$(ConfigurationName)'],
                        'libraries': ['<@(node_engine_libs)'],
                    }],
                ],
                'libraries': [
                    '-lkernel32.lib',
                    '-luser32.lib',
                    '-lgdi32.lib',
                    '-lwinspool.lib',
                    '-lcomdlg32.lib',
                    '-ladvapi32.lib',
                    '-lshell32.lib',
                    '-lole32.lib',
                    '-loleaut32.lib',
                    '-luuid.lib',
                    '-lodbc32.lib',
                    '-lDelayImp.lib',
                    '-l"<(node_lib_file)"'
                ],
                'msvs_disabled_warnings': [
                    # warning C4251: 'node::ObjectWrap::handle_' : class 'v8::Persistent<T>'
                    # needs to have dll-interface to be used by
                    # clients of class 'node::ObjectWrap'
                    4251
                ],
            }, {
                # OS!="win"
                'defines': [
                    '_LARGEFILE_SOURCE',
                    '_FILE_OFFSET_BITS=64'
                ],
            }],
            ['OS in "freebsd openbsd netbsd solaris" or \
         (OS=="linux" and target_arch!="ia32")', {
                'cflags': ['-fPIC'],
            }]
        ]
    }
}

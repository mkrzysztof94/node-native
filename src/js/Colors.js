const Super = module.require('../../lib/src/Super')
class Colors extends Super {
    constructor() {
        super()
        this._codes = Colors.getColorCodes()
        this._modes = Colors.getColorModes()
        this._escape = '\u001b'
        this._mode = this._modes.Background
        this._color = this._codes.Red
    }
    get _reset() {
        return this._escape + '[0m'
    }
    get mode() {
        return this._modes
    }

    setMode(name) {
        if (typeof this._modes[name] === 'undefined') {
            console.warn('Mode not defined')
            return this
        }
        this._mode = this._modes[name]
        return this
    }

    setColor(name) {
        if (typeof this._codes[name] === 'undefined') {
            console.warn('Color not defined')
            return this
        }
        this._color = this._codes[name]
        return this
    }

    print(outStr) {
        return this._escape + '[' + this._mode + this._color + outStr
    }

    static getColorCodes() {
        return new Super({
            default: '0m',
            Black: '30m',
            Red: '31m',
            Green: '32m',
            Yellow: '33m',
            Blue: '34m',
            Purple: '35m',
            Cyan: '36m',
            White: '37m',
            HIBlack: '90m',
            HIRed: '91m',
            HIGreen: '92m',
            HIYellow: '93m',
            HIBlue: '94m',
            HIPurple: '95m',
            HICyan: '96m',
            HIWhite: '97m'
        })
    }

    static getColorModes() {
        return new Super({
            Regular: '0;',
            Bold: '1;',
            Underline: '4;',
            Background: ''
        })
    }
}

module.exports = Colors

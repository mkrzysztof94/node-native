const path = require('path')
const dir = path.resolve(__dirname)
const nodePath = `${dir}/../../nodeV8.8.1/`
const internalErrorsa = module.require(`${nodePath}internal/errors`)
const internalUtila = module.require(`${nodePath}internal/util`)
const internalModulea = module.require(`${nodePath}internal/module`)
process.mainModule = module
module.filename = `${dir}/build/Release/NativePack.node`
require = internalModulea.makeRequireFunction(process.mainModule)
internalModulea.addBuiltinLibsToObject(global)
const { NativPack } = module.require(`${dir}/../build/Release/NativePack.node`)

class NativeAdapter extends NativPack {
    constructor() {
        super()
    }
    cons() {
        console.log(JSON.stringify(this))
    }
    static cons() {
        console.log(JSON.stringify(this))
    }
}

module.exports = {
    NativeAdapter,
    NativPack,
    UI: module.require(`${__dirname}/UI`)
}

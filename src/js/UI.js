const Super = module.require('../../lib/src/Super')
let Keys = module.require('./Keys')
const Colors = module.require('./Colors')
const vm = require('vm')
const event = require('events')

class OutputUI extends Super {
    constructor(outStream) {
        super()
        this.stream = outStream
        this.Colors = new Colors()
        this.Colors.setColor(this.Colors._codes.Red)
    }

    reset() {
        this.stream.write(this.Colors._reset)
    }

    write(str) {
        this.stream.write(str)
    }
}
class InputUI extends Super {
    constructor(inpStream, outputUI) {
        super()
        this.stream = inpStream
        this.outputUI = outputUI
        this.event = new event.EventEmitter()
    }

    run(onDataEvent) {
        this.stream.read()
        this.stream.setRawMode(true)
        this.stream.setEncoding('latin1')
        this.stream.addListener('data', (...args) => onDataEvent(args))
    }

    default(event, ...args) {
        console.log(event, args)
    }

    get eventsList() {
        return ['data', 'error']
    }
}

class UI extends Super {
    constructor() {
        super()
        this.outputUI = new OutputUI(process.stdout)
        this.inputStream = new InputUI(process.stdin, this.OutputStream)
        this._ErrorStream = process.stderr
        this.inputLine = ''
        this._regEx = /(\u001b|\u0003)/u
    }

    run(context) {
        this.inputStream.run(args => this.onData(args))
    }

    onData(data) {
        console.log(data)
        for (let i = 0; i < data.length; i++) {
            if (this._regEx.test(data[i])) {
                this.keyHandle(data[i])
            } else {
                this.inputLine += data[i]
                this.write(data[i])
            }
        }
    }

    clearLine() {
        this.inputLine = ''
        this.write('\r')
    }

    keyHandle(inputKey) {
        switch (inputKey) {
        case Keys.Enter:
            // try {
            //     console.log(
            //         vm.runInContext(this.outputUI.line, this.context)
            //     )
            // } catch (error) {
            //     console.log(error)
            // }
            // this.outputUI.clearLine()
            this.clearLine()
            break
        case Keys.CTRL_C:
            process.exit(0)
            break
        default:
            console.log('code not handled', inputKey)
        }
    }

    write(str) {
        this.outputUI.write(str)
    }
}
var a = new String()

module.exports = UI
